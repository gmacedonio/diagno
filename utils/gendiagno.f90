program gendiagno
  ! Simple program for generating diagno files for testing
  !
  ! Author: Giovanni Macedonio
  !
  ! Date: 26-FEB-2025
  !
  use params
  use m_domain
  implicit none

  integer :: nout = 2
  real :: time    ! This time has format HHMM
  real :: u(nxmax,nymax,nzmax)
  real :: v(nxmax,nymax,nzmax)
  real :: w(nxmax,nymax,nzmax)
  integer :: it,i,j,k

  utmxor = 0.0    ! In km
  utmyor = 0.0    ! In km

  time = 0.0

  nx = 10
  ny = 12
  nz = 14

  dx = 10.0
  dy = 10.0

  do k = 1, nz+1
     cellzb(k) = (k-1)*6.0
  end do

  open(nout,file='diagno.out',status='unknown', form='unformatted')
  tinc = 1.0
  do it = 0, 5
     time = 100*it  ! TIme in hours
     do k = 1, nz
        do j = 1, ny
           do i = 1, nx
              u(i,j,k) = it + 0.01*i + 0.0001*(k-1)
              v(i,j,k) = 100 + it + 0.01*i
              w(i,j,k) = 200 + it + 0.01*i
           end do
        end do
     end do
     call outfil(nout, time, u, v, w)
  end do

end program gendiagno

