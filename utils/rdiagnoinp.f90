program ridiagno
  ! Read the diagno input file
  !
  ! Author: Giovanni Macedonio
  !
  ! Date: 30-NOV-2021
  !
  implicit none
  integer, parameter :: ninp=1
  integer :: narg
  character(len=20) :: title
  character(len=255) :: finp
  integer :: nx, ny, nz
  real :: dxk, dyk
  real :: utmxor, utmyor
  real :: tstart, tend, tinc, zswind, rmin, rmax1, rmax2, rmax3, r1, r2
  real :: critfn, tinf, htfac, divlim, barxy, alpha, terrad
  real, allocatable :: cellzb(:)
  real, allocatable :: fextrp(:)
  real, allocatable :: tgamma(:)
  real, allocatable :: um(:), vm(:)
  integer :: nzprnt, ipr0, ipr1, ipr2, ipr3, ipr4, ipr5, ipr6, ipr7, ikine
  integer :: icalc, ioutd, niter, iobr, numbar, i3dctw, nsmth, iextrp
  integer :: nupper, nwind, infradj
  integer, allocatable :: nintrp(:)
  integer :: i

  finp = 'diagno.inp'

  narg = command_argument_count()
  if(narg >= 1) call get_command_argument(1, finp)

  open(ninp, file=finp, status='old', err=20)

  read(ninp, '(a)', err=20, end=20) title
  read(ninp, *) nx
  read(ninp, *) ny
  read(ninp, *) nz
  read(ninp, *) dxk
  read(ninp, *) dyk
  allocate(cellzb(nz+1))
  read(ninp, *) (cellzb(i),i=1,nz+1) ! Note: nz+1
  read(ninp, *) utmxor
  read(ninp, *) utmyor
  read(ninp, *) tstart
  read(ninp, *) tend
  read(ninp, *) tinc
  read(ninp, *) nwind
  read(ninp, *) nupper
  read(ninp, *) zswind
  read(ninp, *) rmin
  read(ninp, *) rmax1
  read(ninp, *) rmax2
  read(ninp, *) rmax3
  read(ninp, *) r1
  read(ninp, *) r2
  allocate(nintrp(nz))
  read(ninp, *) (nintrp(i),i=1,nz)
  read(ninp, *) nzprnt
  read(ninp, *) ipr0
  read(ninp, *) ipr1
  read(ninp, *) ipr2
  read(ninp, *) ipr3
  read(ninp, *) ipr4
  read(ninp, *) ipr5
  read(ninp, *) ipr6
  read(ninp, *) ipr7
  read(ninp, *) icalc
  read(ninp, *) ioutd
  read(ninp, *) htfac
  read(ninp, *) niter
  read(ninp, *) divlim
  read(ninp, *) iobr
  read(ninp, *) numbar
  read(ninp, *) i3dctw
  read(ninp, *) nsmth
  read(ninp, *) iextrp
  allocate(fextrp(nz))
  read(ninp, *) (fextrp(i),i=2,nz)
  allocate(tgamma(24))    ! 24 hours
  read(ninp, *) (tgamma(i),i=1,8)
  read(ninp, *) (tgamma(i),i=9,16)
  read(ninp, *) (tgamma(i),i=17,24)
  read(ninp, *) critfn
  read(ninp, *) terrad
  read(ninp, *) tinf
  read(ninp, *) infradj
  read(ninp, *) ikine
  read(ninp, *) alpha
  allocate(um(24))    ! 24 hours
  read(ninp, *) (um(i),i=1,8)
  read(ninp, *) (um(i),i=9,16)
  read(ninp, *) (um(i),i=17,24)
  allocate(vm(24))    ! 24 hours
  read(ninp, *) (vm(i),i=1,8)
  read(ninp, *) (vm(i),i=9,16)
  read(ninp, *) (vm(i),i=17,24)
  if(numbar > 0) then
     ! @@@ TO DO
     read(ninp, *) barxy
  end if
  close(ninp)

  do i=1, 24
     write(*, '(i02.2,3(1x,f9.5))') i, um(i), vm(i), sqrt(um(i)**2+vm(i)**2)
  end do

  ! Normal exit
  call exit(0)

  ! Errors
20 write(*,*) 'Error opening file: '//trim(finp)
  call exit(1)

end program ridiagno
