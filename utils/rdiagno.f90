program rdiagno
  ! Read the diagno output file
  !
  ! Author: Giovanni Macedonio
  !
  ! Date: 4-JUL-2023
  !
  use m_dwm
  implicit none
  integer, parameter :: ninp=1
  character(len=255) :: finp
  integer :: narg
  real(8) :: time
  type(dwm) :: wind
  integer :: status
  logical :: horizontal_interpolate = .false.
  logical :: vertical_interpolate = .false.
  real(8) :: xorig, yorig, dx, dy
  real(8), allocatable :: zlayer(:)
  integer :: k, nx, ny, nz
  !
  finp='diagno.out'   ! Default

  narg = command_argument_count()
  if(narg >= 1) call get_command_argument(1, finp)

  ! Set the horizontal domain (also sets horizontal interpoation)
  if(horizontal_interpolate) then
     nx = 21
     ny = 25
     xorig = 0.5
     yorig = 0.5
     dx = 0.5
     dy = 0.5
     call wind%set_horizontal_domain(xorig, yorig, nx, ny, dx, dy, status)
     if(status /= 0) stop 'Error from wind%set_horizontal_domain'
  end if

  ! Set the vertical domain (also sets vertical interpolation)
  if(vertical_interpolate) then
     nz = 7
     allocate(zlayer(nz))
     do k = 1, nz
        zlayer(k) = (k-1)*12.0
     end do
     call wind%set_vertical_domain(nz, zlayer, status)
     if(status /= 0) stop 'Error from wind%set_vertical_domain'
  end if

  ! Open dwm file
  call wind%ropen(finp, status)
  if(status /= 0) stop 'Error from wind%ropen: cannot open dwm file'

  write(*,'(''NX, NY'',2(1x,i3))') wind%nx, wind%ny
  write(*,'(''X0, Y0'',2(1x,f10.2))') wind%xorig, wind%yorig
  write(*,'(''DX, DY'',2(1x,f5.2))') wind%dx, wind%dy
  write(*,'(''NZ, Z'',1x,i3,20(1x,f6.2))') wind%nz, wind%zlayer

  time  = 0d0
  do
     call wind%get(time, status)
     if(status /= 0) exit
     ! Next time step
     write(*,'(f6.0,5(1x,f9.5))') time, wind%u(1,1,1,1), wind%u(2,1,1,1), &
          wind%u(3,1,1,1), wind%u(1,1,2,1), wind%u(1,1,3,1)
     time = time + 900d0
  end do

  call wind%free

end program rdiagno
