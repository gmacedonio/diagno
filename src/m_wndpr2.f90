module m_wndpr2

contains

  SUBROUTINE WNDPR2(U,V)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  WNDPR2
    !
    use m_domain
    use m_wndlpt
    implicit none

    real :: U(:,:,:), V(:,:,:)

    integer :: k
    !
    !     WNDPR2 PRINTS OUT U & V WIND FIELD AT EACH LAYER
    !
    !     INPUTS:  U (R ARRAY) - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY) - GRIDDED Y-DIRECTION WIND COMPONENTS
    !
    do k=1,nzprnt
       write(iwr,10) k
       write(iwr,11)
       call wndlpt(u(1:nx,1:ny,k))
       write(iwr,20) k
       write(iwr,11)
       call wndlpt(v(1:nx,1:ny,k))
    end do
    return
10  FORMAT(//,5X,'X-COMPONENT OF WIND (U) AT LEVEL = ',I4,5X,'(M/SEC)')
11  FORMAT(5X,49('-'))
    !     12 FORMAT(5X,57('-'))
20  FORMAT(//,5X,'Y-COMPONENT OF WIND (V) AT LEVEL = ',I4,5X,'(M/SEC)')
  END SUBROUTINE WNDPR2

end module m_wndpr2
