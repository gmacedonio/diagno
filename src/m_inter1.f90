module m_inter1

contains

  SUBROUTINE INTER1(STDAT1,NST,XST,YST,RS,IS,WORK,FIELD1,IFLAG)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  INTERP  S.DOUGLAS, SAI
    !
    use m_domain
    use m_barier
    implicit none
    !
    integer :: nst, iflag
    integer :: is(:)
    real :: xst(:), yst(:), rs(:), work(:)
    real :: stdat1(:,:)
    real :: field1(:,:,:)
    !
    integer :: i, j, k, l, m, n, nk, nmin, nn
    integer :: js(maxval(nintrp))
    real :: fmin, ok
    real :: rsum, rsx, rsy, wt, x, y

    !
    !       COMPUTES 3-D WIND FIELD FROM STATION DATA
    !       BY THE ERT INTERPOLATION SCHEME
    !
    !     INPUTS:  STDAT1 (R ARRAY) - WIND COMPONENT STATION DATA
    !              NST (I)          - NUMBER OF STATIONS
    !              XST (R ARRAY)    - LOCAL X-COORDINATES OF STATIONS
    !              YST (R ARRAY)    - LOCAL Y-COORDINATES OF STATIONS
    !              RMIN (R)         - MINIMUM RADIUS OF INFLUENCE
    !              RMAX1 (R)        - MAXIMUM RADIUS OF INFLUENCE
    !              NINTRP(I)        - NO. OF STATIONS USED IN THE
    !                                 INTERPOLATION TO A GRID POINT
    !              IFLAG (I)        - NO. OF BARRIERS
    !
    !     OUTPUTS:  FIELD1 (R ARRAY) - INTERPOLATED WIND COMPONENT ARRAY
    !
    !     CONVERT GRID SPACING TO KM
    !
    DXK=DX*0.001
    DYK=DY*0.001
    !
    DO J=1,NY
       DO I=1,NX
          X = (I-0.5)*DXK
          Y = (J-0.5)*DYK
          rs(1:nst) = edit
          DO L=1,NST
             IF(IFLAG.LE.0) GO TO 90
             !
             !     CHECK FOR BARRIERS TO INTERPOLATION
             !
             CALL BARIER(X,Y,XST(L),YST(L),OK)
             IF(OK.LE.0.) GO TO 115
             !
             !     SKIP STATION IF NO DATA AT ANY ELEVATION
             !
90           DO K=1,NZ
                IF(STDAT1(K,L).LT.EDITL) GO TO 110
             end DO
             GO TO 115
             !
             !     COMPUTE DISTANCE TO STATIONS LESS THAN RMAX AWAY
             !
110          RSX = X - XST(L)
             RSY = Y - YST(L)
             RS(L) = RSX**2 + RSY**2
             RS(L) = SQRT(RS(L))
             IF(RS(L).LT.RMIN) RS(L)=RMIN
             IF(RS(L) .LE. RMAX1)  GO TO 115
             RS(L) = EDIT
115          CONTINUE
          end DO
          !
          !     ORDER STATIONS
          !
          DO NN = 1,NST
             IS(NN)=IEDIT
          end DO
          work(1:nst) = rs(1:nst)
          N = 0
          DO L = 1,NST
             CALL FMINF(WORK,NST,FMIN,NMIN)
             WORK(NMIN) = EDIT + L
             IS(L) = NMIN
             IF(FMIN.GT.EDITL) exit
             N = N + 1
          end DO
          IF(N.GT.0)  GO TO 150
          WRITE(IWR,2064) I,J
          call exit(1)
150       CONTINUE
          DO K=1,NZ
             !
             !     FIND SUM OF DISTANCES TO STATIONS WITH DATA FOR THIS LEVEL
             !
             RSUM = 0.
             NK = 0
             DO M=1,N
                L=IS(M)
                IF(STDAT1(K,L).GT.EDITL) cycle
                IF(K.GT.1)  GO TO 155
                IF (RS(L) .GT. RMAX1) cycle
                RSUM = RSUM + 1./RS(L)**2
                GO TO 158
155             RSUM = RSUM + 1./RS(L)
158             CONTINUE
                NK = NK + 1
                JS(NK) = IS(M)
                IF(NK .EQ. NINTRP(K)) GO TO 165
             end DO
             IF(NK .GT. 0) GO TO 165
             WRITE(IWR,2065) I,J,K
             call exit(1)
165          CONTINUE
             !
             !     FIND THE INTERPOLATION WEIGHTING FACTOR FOR EACH VALID STATION
             !
             DO M = 1,NK
                L=JS(M)
                IF(STDAT1(K,L) .GT. EDITL) cycle
                IF(K.GT.1)  GO TO 170
                WT = 1./((RS(L)**2)*RSUM)
                GO TO 175
170             WT = 1./(RS(L)*RSUM)
                !
                !     APPLY WEIGHTING FACTOR
                !
175             CONTINUE
                FIELD1(I,J,K) = FIELD1(I,J,K) + STDAT1(K,L)*WT
             end DO
          end DO
       end DO
    end DO
    RETURN
2064 FORMAT('JOB ABORTED--THERE ARE NO CLOSE STATIONS FOR I = ', &
         I4,' J = ',I4,' IN SUBROUTINE INTERP')
2065 FORMAT('JOB ABORTED--THERE ARE NO CLOSE STATIONS FOR I = ', &
         I4,' J = ',I4,' K = ',I4,' IN SUBROUTINE INTERP')
  END SUBROUTINE INTER1

end module m_inter1
