subroutine readgrd(fgrd,xo,yo,dx,dy,nx,ny,nxg,nyg,g)
  !***************************************************************
  !*
  !*   Reads data from a surfer GRD ASCII file
  !*
  !***************************************************************
  implicit none
  integer, parameter :: RP = kind(1.0)  ! Default REAL*4
  !
  character(len=*), intent(in) :: fgrd     ! Input GRD file name
  integer, intent(in) :: nx,ny             ! Number of pixel (output matrix)
  integer, intent(in) :: nxg,nyg           ! Shape of the output matrix
  real(RP), intent(in) :: xo,yo            ! Origin (left-bottom corner)
  real(RP), intent(in) :: dx,dy            ! Pixel dimensions (output matrix)
  real(RP), intent(out) :: g(nxg,nyg)      ! Matrix (output)
  !
  real(RP) :: xf,yf    ! Coordinates of the up-right corner (output matrix)
  real(RP) :: x,y      ! Temporary variable
  integer  :: ix,iy
  integer  :: ixgrd,iygrd
  real(RP) :: get_val_from_grd
  !
  ! Input GRD matrix
  integer(kind=2)  :: nxgrd,nygrd
  character(len=4) :: magic
  real(kind=8)     :: xogrd,yogrd,xfgrd,yfgrd
  real(kind=8)     :: zogrd,zfgrd
  real(kind=4), allocatable :: grd(:,:)
  real(RP) :: dxgrd,dygrd         ! Pixel dimensions of the input matrix
  !
  ! Types for GRD file
  !
  !*** Computes xf and yf (upper-right corner)
  !
  xf = xo + nx*dx
  yf = yo + ny*dy
  !
  !*** Reads the file (ASCII version)
  !
  open(90,FILE=TRIM(fgrd),STATUS='old',err=100)
  read(90,'(a4)',err=101) magic
  if(TRIM(magic) /= 'DSAA') then
     write(*,*) 'Topography not in ASCII grd format'
     call exit(1)
  end if
  read(90,*,err=101) nxgrd,nygrd
  read(90,*,err=101) xogrd,xfgrd   ! Bottom-left pixel center
  read(90,*,err=101) yogrd,yfgrd
  read(90,*,err=101) zogrd,zfgrd
  !
  dxgrd = real((xfgrd-xogrd)/(nxgrd-1), kind=rp)
  dygrd = real((yfgrd-yogrd)/(nygrd-1), kind=rp)
  !
  !*** Checks that the computational domain is within the regional GRD file
  !
  if(xogrd - 0.5*dxgrd > xo) then
     write(*,'(''***Error: Domain x0 is outside the GRD file'')')
     write(*,*) 'Domain x0: ',xo
     write(*,*) 'Topog. x0: ',real(xogrd - 0.5*dxgrd)
     call exit(1)
  end if
  if(xfgrd + 0.5*dxgrd < xf) then
     write(*,'(''***Error: Domain xf is outside the GRD file'')')
     write(*,*) 'Domain xf: ',xf
     write(*,*) 'Topog. xf: ',real(xfgrd + 0.5*dxgrd)
     call exit(1)
  end if
  if(yogrd - 0.5*dygrd > yo) then
     write(*,'(''***Error: Domain yo is outside the GRD file'')')
     write(*,*) 'Domain yo: ',yo
     write(*,*) 'Topog. yo: ',real(yogrd - 0.5*dygrd)
     call exit(1)
  end if
  if(yfgrd + 0.5*dygrd < yf) then
     write(*,'(''***Error: Domain yf is outside the GRD file'')')
     write(*,*) 'Domain yf: ',yf
     write(*,*) 'Topog. yf: ',real(yfgrd + 0.5*dygrd)
  end if
  !
  allocate(grd(nxgrd,nygrd))
  !
  !
  do iygrd=1,nygrd
     read(90,*,err=101) (grd(ixgrd,iygrd),ixgrd=1,nxgrd)
  end do
  close(90)
  !
  !*** Interpolates
  !
  do iy = 1,ny
     y = yo + (iy-0.5)*dy
     do ix = 1,nx
        x = xo + (ix-0.5)*dx
        g(ix,iy)=get_val_from_grd(xogrd,yogrd,nxgrd,nygrd,dxgrd,dygrd,grd,x,y)
     end do
  end do
  !
  !*** Releases memory
  !
  deallocate(grd)
  return
  !
  !*** List of errors
  !
100 write(*,*) 'Error opening GRD file: '//TRIM(fgrd)
  call exit(1)
101 write(*,*) 'Error reading GRD file: '//TRIM(fgrd)
  call exit(1)
  !
end subroutine readgrd
!
!
function get_val_from_grd(xogrd,yogrd,nxgrd,nygrd,dxgrd,dygrd,grd,x,y)
  !***********************************************************
  !*
  !*   Interpolates value at the (x,y) point
  !*
  !***********************************************************
  implicit none
  integer, parameter :: RP = kind(1.0)  ! Default REAL*4
  !
  real(rp) :: get_val_from_grd
  real(rp) :: dxgrd,dygrd
  real(rp) :: x,y,xgrd,ygrd
  !
  ! Types for GRD file
  integer(kind=2) :: nxgrd,nygrd
  real(kind=8) :: xogrd,yogrd
  real(kind=4) :: grd(nxgrd,nygrd)
  !
  logical  :: xfound,yfound
  integer  :: iygrd,ixgrd,ixpt,iypt
  real(rp) :: s,t,st,gshape(4)
  !
  xfound = .false.
  yfound = .false.
  !
  do ixgrd = 1,nxgrd-1
     xgrd = real(xogrd + (ixgrd-1)*dxgrd, kind=rp)
     if((x >= xgrd).and.(x <= (xgrd+dxgrd))) then
        ixpt   = ixgrd
        xfound = .true.
     end if
  end do
  if(.not.xfound) then
     write(*,*) 'get_val_from_grd: x-value not found'
     write(*,*) 'Invalid domain (x)'
     call exit(1)
  end if
  !
  do iygrd = 1,nygrd-1
     ygrd = real(yogrd + (iygrd-1)*dygrd, kind=rp)
     if((y >= ygrd).and.(y <= (ygrd+dygrd))) then
        iypt   = iygrd
        yfound = .true.
     end if
  end do
  if(.not.yfound) then
     write(*,*) 'get_val_from_grd: y-value not found'
     write(*,*) 'Invalid domain (y)'
     call exit(1)
  end if
  !
  !*** Interpolates
  !
  s = real((x-(xogrd+(ixpt-1)*dxgrd))/dxgrd, kind=rp)  ! parameter s in (0,1)
  s = 2.0_rp*s - 1.0_rp                                ! parameter s in (-1,1)
  t = real((y-(yogrd+(iypt-1)*dygrd))/dygrd, kind=rp)  ! parameter t in (0,1)
  t = 2.0_rp*t - 1.0_rp                                ! parameter t in (-1,1)
  st = s*t
  !
  gshape(1)=(1.0_rp-t-s+st)*0.25_rp                    ! 4         3
  gshape(2)=(1.0_rp-t+s-st)*0.25_rp                    !
  gshape(3)=(1.0_rp+t+s+st)*0.25_rp                    !
  gshape(4)=(1.0_rp+t-s-st)*0.25_rp                    ! 1         2
  !
  get_val_from_grd = gshape(1)*grd(ixpt  ,iypt  ) +  &
       gshape(2)*grd(ixpt+1,iypt  ) +  &
       gshape(3)*grd(ixpt+1,iypt+1) +  &
       gshape(4)*grd(ixpt  ,iypt+1)
  !
  return
end function get_val_from_grd
