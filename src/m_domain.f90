!> Domain of diagno
module m_domain
  implicit none
  integer :: nx, ny, nz, nzp1
  integer :: nwind, nupper
  integer :: nzprnt, icalc, ioutd
  integer :: ird, iwr
  integer :: ipr0, ipr1, ipr2, ipr3,ipr4,ipr5,ipr6,ipr7
  integer :: niter, numbar,i3dctw,nsmth,iextrp,iobr
  integer :: ifradj, ikine
  integer, allocatable :: nintrp(:)
  real :: dx, dy
  real :: utmxor, utmyor, dxk, dyk    ! In km
  real :: tstart, tend                ! Tiem start, end
  real :: tinc                        ! Time increment (hours)
  real :: tinf
  real :: zswind
  real :: rmin,rmax1,rmax2,rmax3,r1,r2
  real :: htfac, divlim
  real :: critfn, terrad, alpha
  real :: edit, editl
  integer :: iedit, ieditl
  real, allocatable :: dz(:)          ! NZ
  real, allocatable :: cellzb(:)      ! Cell boundaries (NZ+1)
  real, allocatable :: fextrp(:)

  real :: TGAMMA(24), beta2(24), um(24), vm(24)   ! @@@@@@@@@@@@@@@@@@@@@

  ! barriers
  integer :: ifin
  real, allocatable :: barxy(:,:), slpin(:,:)

contains

  SUBROUTINE SETUP
    !     READ SETUP PARAMATERS
    implicit none
    integer :: i, n
    READ(IRD,*) NX
    READ(IRD,*) NY
    READ(IRD,*) NZ
    READ(IRD,*) DXK
    READ(IRD,*) DYK
    NZP1=NZ+1
    allocate(cellzb(nzp1))
    READ(IRD,*) (CELLZB(I),I=1,NZP1)
    READ(IRD,*) UTMXOR
    READ(IRD,*) UTMYOR
    READ(IRD,*) TSTART
    READ(IRD,*) TEND
    READ(IRD,*) TINC
    READ(IRD,*) NWIND
    READ(IRD,*) NUPPER
    READ(IRD,*) ZSWIND
    READ(IRD,*) RMIN
    READ(IRD,*) RMAX1
    READ(IRD,*) RMAX2
    READ(IRD,*) RMAX3
    READ(IRD,*) R1
    READ(IRD,*) R2
    allocate(nintrp(nz))
    READ(IRD,*) (NINTRP(I), I = 1,NZ)
    READ(IRD,*) NZPRNT
    READ(IRD,*) IPR0
    READ(IRD,*) IPR1
    READ(IRD,*) IPR2
    READ(IRD,*) IPR3
    READ(IRD,*) IPR4
    READ(IRD,*) IPR5
    READ(IRD,*) IPR6
    READ(IRD,*) IPR7
    READ(IRD,*) ICALC
    READ(IRD,*) IOUTD
    READ(IRD,*) HTFAC
    READ(IRD,*) NITER
    READ(IRD,*) DIVLIM
    READ(IRD,*) IOBR
    READ(IRD,*) NUMBAR
    READ(IRD,*) I3DCTW
    READ(IRD,*) NSMTH
    READ(IRD,*) IEXTRP
    allocate(fextrp(nz))
    fextrp = 0.0  ! Clear
    read(ird,*) (fextrp(i), i=2,nz)

    if(i3dctw .eq. 1) then
       !
       !     READ DIAGNOSTIC PARAMETERS
       !
       READ(IRD,*) (TGAMMA(I),I=1,8)
       READ(IRD,*) (TGAMMA(I),I=9,16)
       READ(IRD,*) (TGAMMA(I),I=17,24)
       !
       !     STORE THE SIGN OF GAMMA IN BETA2 ARRAY
       !
       DO I = 1,24
          IF (TGAMMA(I) .LT. 0.) BETA2(I) = 1.
          IF (TGAMMA(I) .EQ. 0.) BETA2(I) = 0.
          IF (TGAMMA(I) .GT. 0.) BETA2(I) = -1.
       end DO
       !
       !     CONVERT GAMMA TO DEG-K/KM
       !
       ! Do not convert: Was already in K/km
       ! DO I = 1,24
       !   TGAMMA(I) = TGAMMA(I)/1000.
       ! end DO
       !
       READ(IRD,*) CRITFN
       READ(IRD,*) TERRAD
       READ(IRD,*) TINF
       READ(IRD,*) IFRADJ
       READ(IRD,*) IKINE
       READ(IRD,*) ALPHA
       READ(IRD,*) (UM(I), I = 1,8)
       READ(IRD,*) (UM(I), I = 9,16)
       READ(IRD,*) (UM(I), I = 17,24)
       READ(IRD,*) (VM(I), I = 1,8)
       READ(IRD,*) (VM(I), I = 9,16)
       READ(IRD,*) (VM(I), I = 17,24)
    end IF
    !
    !     READ IN BARRIERS TO INTERPOLATION (MAX=20)
    IF(NUMBAR .GT. 0) then
       !
       !     READ IN BARRIER SEGMENT END POINT COORDINATES (UTM, KM)
       !
       allocate(barxy(4,numbar))
       allocate(slpin(2,numbar))
       do n = 1, numbar
          !     READ(IRD,1020) (BARXY(I,N),I=1,4)
          READ(IRD,*) (BARXY(I,N),I=1,4)
          !
          !     CONVERT TO LOCAL COORDINATES
          !
          do i = 1, 4, 2
             barxy(i,n)   = barxy(i,n)   - utmxor
             barxy(i+1,n) = barxy(i+1,n) - utmyor
          end do
       end do
    end if


    ! Allocate memory
    allocate(dz(nz))

  END SUBROUTINE SETUP

end module m_domain
