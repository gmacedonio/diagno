module m_divpr

contains

  SUBROUTINE DIVPR(DIV)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  DIVPR
    !
    use m_domain
    use m_wndlpt
    implicit none

    real :: DIV(:,:,:)
    integer :: k
    !
    !     DIVPR PRINTS OUT DIVERGENCE FIELD AT EACH LAYER .
    !
    !     INPUTS:  DIV (R ARRAY) - GRIDDED 3-D DIVERGENCE
    !
    DO K=1,NZPRNT
       WRITE(IWR,40) K
40     FORMAT(//,5X,'DIVERGENCE AT LEVEL = ',I4)
       WRITE(IWR,41)
41     FORMAT(5X,24('-'))
       CALL WNDLPT(DIV(1:nx,1:ny,K))
    end DO
    RETURN
  END SUBROUTINE DIVPR

end module m_divpr
