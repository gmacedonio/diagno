module m_outfil

contains

  subroutine outfil(ifile,time,u,v,w)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  OUTFIL    S.DOUGLAS, SAI
    !
    !     Changes:
    !     a) Prints dx and dy in meters (instead of km)
    !     b) Prints origin of domain utmxor,utmyor (in meters)
    !     c) Added a new record for printind dz(1:nz)
    !     d) Added a new block for printing vz
    !     e) Output Zref (zswind)
    !     f) Output Tz0  (tinf)
    !
    use m_domain

    REAL XORIG,YORIG
    real :: U(:,:,:), V(:,:,:), W(:,:,:)
    !
    !     WRITES OUTPUT FILE OF WIND COMPONENTS
    !
    !     INPUTS:  IFILE (I)   - UNIT NO OF OUTPUT FILE
    !     TIME (R)    - SIMULATION INITIAL AND FINAL TIME
    !     U (R ARRAY) - GRIDDED X-DIRECTION WIND COMPONENTS
    !     V (R ARRAY) - GRIDDED Y-DIRECTION WIND COMPONENTS
    !     W (R ARRAY) - GRIDDED VERTICAL WIND COMPONENTS
    !
    ihour = int(time/100.)            ! This the hour (0-23)
    !
    write(ifile) time                 ! Format is HHMM
    write(ifile) tinc                 ! This is the increment in hours
    !
    xorig = 1000.*utmxor              ! Convert origin in meters
    yorig = 1000.*utmyor
    write(ifile) xorig,yorig
    !
    write(ifile) nx,ny,nz
    write(ifile) dx,dy
    write(ifile) (cellzb(i),i=1,nz+1) ! Note: nz+1
    write(ifile) zswind               ! Z of wind measurement (from ground)
    write(ifile) tinf                 ! Soil temperature     (K)
    write(ifile) tgamma(ihour+1)      ! Temperature gradient (K/m)
    !
    !     VX
    !
    do k=1,nz
       do j=1,ny
          write(ifile) (u(i,j,k),i=1,nx)
       enddo
    enddo
    !
    !     VY
    !
    do k=1,nz
       do j=1,ny
          write(ifile) (v(i,j,k),i=1,nx)
       enddo
    enddo
    !
    !     VZ  (This block is new)
    !
    do k=1,nz                               ! Note: k=1,nz
       do j=1,ny
          write(ifile) (w(i,j,k+1),i=1,nx)  ! WARN: Don't print vz(i,j,1)=0
       enddo
    enddo
    !
    return
  end subroutine outfil

end module m_outfil
