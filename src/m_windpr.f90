module m_windpr

contains

  SUBROUTINE WINDPR(U,V,W)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  WINDPR
    !
    use m_domain
    use m_wndlpt
    implicit none

    real :: U(:,:,:), V(:,:,:), W(:,:,:)
    integer :: k, km
    !
    !     WINDPR PRINTS OUT WIND FIELD AT EACH LAYER .
    !
    !     INPUTS:  U (R ARRAY) - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY) - GRIDDED Y-DIRECTION WIND COMPONENTS
    !              W (R ARRAY) - GRIDDED VERTICAL WIND COMPONENTS
    !
    DO K=1,NZPRNT
       IF(K.EQ.1) GO TO 50
       KM = K-1
       WRITE(IWR,30) KM,K
       WRITE(IWR,12)
       CALL WNDLPT(W(1:nx,1:ny,K))
50     WRITE(IWR,10) K
       WRITE(IWR,11)
       CALL WNDLPT(U(1:nx,1:ny,K))
       WRITE(IWR,20) K
       WRITE(IWR,11)
       CALL WNDLPT(V(1:nx,1:ny,K))
    end do
    RETURN
10  FORMAT(/,5X,'X-COMPONENT OF WIND (U) AT LEVEL = ',I4,5X,'(M/SEC)')
11  FORMAT(5X,49('-'))
12  FORMAT(5X,59('-'))
20  FORMAT(/,5X,'Y-COMPONENT OF WIND (V) AT LEVEL = ',I4,5X,'(M/SEC)')
30  FORMAT(/,5X,'Z-COMPONENT OF WIND (W) BETWEEN LEVELS',I4, &
         ' & ',I2,5X,'(M/SEC)')
  END SUBROUTINE WINDPR

end module m_windpr
