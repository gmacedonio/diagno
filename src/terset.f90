module m_terset

contains

SUBROUTINE TERSET(HTOPO,HMAX)
  !
  !     DIAGNO  VERSION 1.1  LEVEL 900221  TERSET    S.DOUGLAS, SAI
  !
  use m_domain
  implicit none

  real :: HTOPO(:,:), HMAX(:,:)
  !
  !     THIS SUBROUTINE DETERMINES THE HEIGHT (HMAX) OF THE HIGHEST
  !     TERRAIN WITHIN A RADIUS = TERRAD (KM) OF THE POINT I,J
  !
  !
  !     INPUT:  HTOPO (R ARRAY) - GRIDDED TERRAIN HEIGHTS
  !             TERRAD (R)      - RADIUS OF SEARCH FOR HIGHEST TERRAIN
  !                               HEIGHTS
  !
  !     OUTPUT:  HMAX (R ARRAY) - MAXIMUM TERRAIN HEIGHT WITHIN A
  !                               GIVEN RADIUS
  !
  DXK=DX*0.001
  DYK=DY*0.001
  IRANGE=NINT(TERRAD/DXK)
  JRANGE=NINT(TERRAD/DYK)
  IBNDY=NX-IRANGE
  JBNDY=NY-JRANGE
  !
  DO J=1,NY
     DO I=1,NX
        HMAX(I,J)=HTOPO(I,J)
     end DO
  end DO
  DO J=1,NY
     L1=1
     IF (J .GT. JRANGE) L1=J-JRANGE
     L2=NY
     IF (J .LT. JBNDY) L2=J+JRANGE
     DO I=1,NX
        K1=1
        IF (I .GT. IRANGE) K1=I-IRANGE
        K2=NX
        IF (I .LT. IBNDY) K2=I+IRANGE
        DO L=L1,L2
           DO K=K1,K2
              XDIST=(K-I)*DXK
              YDIST=(L-J)*DYK
              DIST=SQRT(XDIST**2+YDIST**2)
              IF (DIST .GT. TERRAD) cycle
              HMAX(I,J)=AMAX1(HMAX(I,J),HTOPO(K,L))
           end DO
        end DO
     end DO
  end DO
  RETURN
END SUBROUTINE TERSET

end module m_terset
