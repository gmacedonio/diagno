PROGRAM PREUPR
  !
  !     THIS PROGRAM PREPROCESSES THE UPPER LEVEL WIND DATA BY
  !     VERTICAL AND TEMPORAL INTERPOLATION
  !     FOR: IOPT = 1 DATA ARE AVERAGED WITHIN EACH VERTICAL CELL
  !          IOPT = 2 DATA ARE LINEARLY INTERPOLATED TO CELL CENTER
  !                   HEIGHTS
  !
  PARAMETER (MAXST = 100, NUMHR = 24, MAXLEV =50, MAXK = 15, &
       MAXKP1 = MAXK + 1, MAXRD = 24)
  CHARACTER(len=3) :: NAMMET,NAMDEG,NAMMPS,NAMMPH,NAMKTS,LUNITS
  CHARACTER(len=3) :: NAMFET
  CHARACTER(len=4) :: NAMST(MAXST),LSTAT,LAST
  character(512) wdir
  COMMON/P1/ CELLZB(MAXKP1), UTMXST(MAXST), UTMYST(MAXST), &
       MREAD(MAXST), NREAD(MAXST), ELEV(MAXST), &
       JHOUR(MAXRD,MAXST), HOUR(MAXRD, MAXST), &
       STADTA(MAXLEV,3,MAXRD,MAXST), NHTS(MAXRD,MAXST), &
       LEVEL(MAXLEV),STATMP(MAXK,2,MAXRD,MAXST), &
       WNDATA(MAXK,2,MAXRD,MAXST), NCOUNT(MAXK,MAXRD,MAXST), &
       DZ(MAXK), CELLZC(MAXK)
  !
  DATA LAST/'LAST'/
  DATA NAMMET/'MET'/, NAMDEG/'DEG'/, NAMMPS/'MPS'/, &
       NAMMPH/'MPH'/, NAMKTS/'KTS'/, NAMFET/'FET'/
  !
  !     DEFINE UPPER AND LOWER LIMITS OF CONTROL PARAMETERS
  !
  DATA MINHR/-1200/, MAXHR/3500/
  DATA MINPNT/0/, MAXPNT/360/
  DATA MINSPD/0/,MAXSPD/500/
  !
  !     SET LOGICAL UNIT NUMBERS
  !
  IRD = 5
  IWR = 6
  IFILE = 7
  if(command_argument_count() >= 1) then
     call get_command_argument(1, wdir)
  else
     wdir = '.'
  end if
  !
  !     OPEN FILES
  !
  OPEN(UNIT=5,FILE=TRIM(wdir)//'/'//'preupr.dat',STATUS='OLD')
  !RS   OPEN(UNIT=6,FILE='PREUPR.PRT',STATUS='NEW')
  OPEN(UNIT=7,FILE=TRIM(wdir)//'/'//'upper.dat',STATUS='unknown')
  !
  !     READ CONTROL INFORMATION
  !
  !$$$      READ(IRD,1010) NSTA
  !$$$      READ(IRD,1010) LEVELS
  !$$$      READ(IRD,1010) NSTRHR
  !$$$      READ(IRD,1010) NENDHR
  !$$$      READ(IRD,1020) TDIF
  !$$$      READ(IRD,1010) NCELL
  !$$$      READ(IRD,1030) (CELLZB(K),K=1,NCELL+1)
  !$$$      READ(IRD,1010) KYEAR
  !$$$      READ(IRD,1010) KMONTH
  !$$$      READ(IRD,1010) KDAY
  !$$$      READ(IRD,1010) IOPT
  !
  READ(IRD,*) NSTA
  READ(IRD,*) LEVELS
  READ(IRD,*) NSTRHR
  READ(IRD,*) NENDHR
  READ(IRD,*) TDIF
  READ(IRD,*) NCELL
  READ(IRD,*) (CELLZB(K),K=1,NCELL+1)
  READ(IRD,*) KYEAR
  READ(IRD,*) KMONTH
  READ(IRD,*) KDAY
  READ(IRD,*) IOPT
  !
  !     INITIALIZE ARRAYS
  !
  DO I=1,MAXST
     MREAD(I)=0
     NREAD(I)=0
     DO J=1,MAXRD
        NHTS(J,I)=0
     end DO
  end DO

  !
  !     WRITE OUT OPTION INFO
  !
  IF (IOPT .EQ. 1) WRITE(IWR,1140)
  IF (IOPT .EQ. 2) WRITE(IWR,1150)
  !
  !     READ STATION NAMES, COORDINATES AND ELEVATIONS
  !
  DO N=1,NSTA
     READ(IRD,1040) NAMST(N),UTMXST(N),UTMYST(N),ELEV(N)
  end DO
  !
  !     READ STATION DATA
  !
15 READ(IRD,1050,END=20) LYEAR,LMONTH,LDAY,LSTAT,LHOUR,LUNITS,(LEVEL(M),M=1,LEVELS)
  !
  !     CHECK DATE -- WRITE WARNING MESSAGE IF MISMATCH
  !     SET JULIAN DATES : LJULIAN, KJULIAN
  !
  CALL JULDAY(KJULIAN,KYEAR,KMONTH,KDAY,2)
  CALL JULDAY(LJULIAN,LYEAR,LMONTH,LDAY,2)
  !
  KDAYM1 = KJULIAN - 1
  KDAYP1 = KJULIAN + 1
  IF (LJULIAN .EQ. KDAYM1) THEN
     LJULIAN = KJULIAN
     LH = LHOUR/100
     LM = MOD(LHOUR,100)
     LH = LH - 23
     LM = LM - 60
     IF (LM .EQ. -60) THEN
        LM = 0
        LH = LH - 1
     ENDIF
     LHOUR = LH * 100 + LM
  ENDIF
  IF (LJULIAN .EQ. KDAYP1) THEN
     LJULIAN = KJULIAN
     LHOUR = LHOUR + 2400
  ENDIF
  !
  CALL JULDAY(LJULIAN,LYEAR,LMONTH,LDAY,1)
  !
  IF (LDAY .EQ. KDAY) GO TO 80
  WRITE (IWR,1060) LSTAT,LHOUR,LYEAR,LMONTH,LDAY,KYEAR,KMONTH,KDAY
  GO TO 15
  !
  !     CHECK TIME
  !
80 IF (LHOUR .GE. MINHR .AND. LHOUR .LE. MAXHR) GO TO 40
  WRITE(IWR,1070) LHOUR,LSTAT,MINHR,MAXHR
  GO TO 15
  !
  !     IDENTIFY STATION
  !
40 DO K=1,NSTA
     KC=K
     IF (NAMST(K) .EQ. LSTAT) GO TO 60
  end DO
  !
  !     STATION NOT FOUND -- WRITE WARNING MESSAGE
  !
  WRITE(IWR,1080) LSTAT
  GO TO 15
  !
  !     INCREMENT DATA COUNTER AND CHECK FOR OVERFLOW
  !     THERE ARE 3 RECORDS PER STATION (DEG, HEIGHT, WS)
  !
60 MSTA=KC
  MREAD(MSTA)=MREAD(MSTA)+1
  IF (MOD(MREAD(MSTA)-1,3) .NE. 0) GO TO 90
  NREAD(MSTA)=NREAD(MSTA)+1
  !
  !     CHECK LIMITS OF OTHER VARIABLES
  !
90 NDATA=NREAD(MSTA)
  JHOUR(NDATA,MSTA)=LHOUR
  HOUR(NDATA,MSTA)=FLOAT(LHOUR/100)+FLOAT(MOD(LHOUR,100))/60.
  !
  DO M=1,LEVELS
     IF (LUNITS .EQ. NAMMET .OR. LUNITS .EQ. NAMFET) GO TO 110
     IF (LUNITS .EQ. NAMDEG) GO TO 120
     IF (LUNITS .EQ. NAMMPS .OR. LUNITS .EQ. NAMKTS .OR. &
          LUNITS .EQ. NAMMPH) GO TO 130
     !
     !     THIS IS NOT A WIND STATION
     !
     WRITE(IWR,1090)
     GO TO 15
     !
     !      CHECK HEIGHT READING
     !
110  IF (M .EQ. 1 .OR. LEVEL(M) .GT. 0) GO TO 140
     IF (LEVEL(M) .LT. 0) WRITE(IWR,1100) LSTAT,M,LEVEL(M)
     GO TO 15
140  IF (LUNITS .EQ. NAMFET) LEVEL(M) = NINT(FLOAT(LEVEL(M)) * 0.3048)
     STADTA(M,1,NDATA,MSTA)=FLOAT(LEVEL(M))-ELEV(MSTA)
     NHTS(NDATA,MSTA)=NHTS(NDATA,MSTA)+1
     cycle
     !
     !     CHECK DIRECTION READING
     !
120  IF (LEVEL(M) .GE. MINPNT .AND. LEVEL(M) .LE. MAXPNT) GO TO 160
     WRITE(IWR,1110) LSTAT,M,LEVEL(M)
     GO TO 15
160  STADTA(M,2,NDATA,MSTA)=FLOAT(LEVEL(M))
     cycle
     !
     !     CHECK SPEED READING
     !
     !     CONVERT TO MPS * 10
     !
130  IF (LUNITS .EQ. NAMKTS) LEVEL(M) = NINT(FLOAT(LEVEL(M)) * 0.5144)
     IF (LUNITS .EQ. NAMMPH) LEVEL(M) = NINT(FLOAT(LEVEL(M)) * 0.4470)
     IF (LEVEL(M) .GE. MINSPD .AND. LEVEL(M) .LE. MAXSPD) GO TO 170
     WRITE(IWR,1120) LSTAT,M,LEVEL(M)
     GO TO 15
     !
     !     CONVERT TO MPS
     !
170  STADTA(M,3,NDATA,MSTA)=FLOAT(LEVEL(M))*0.10
     !
  end DO
  GO TO 15
20 CONTINUE
  !
  !     CHECK THAT EACH STATION HAS AT LEAST ONE DATUM
  !
  DO J=1,NSTA
     IF (NREAD(J) .GT. 0) cycle
     WRITE(IWR,1130) NAMST(J)
     GO TO 999
  end DO
  !
  !     CONVERT TO WIND COMPONENTS
  !
  RPD=3.14159/180.
  DO K=1,NSTA
     NDATA=NREAD(K)
     DO J=1,NDATA
        DO M=1,NHTS(J,K)
           ANGLE=STADTA(M,2,J,K)*RPD
           SPEED=STADTA(M,3,J,K)
           STADTA(M,2,J,K)=-SPEED*SIN(ANGLE)
           STADTA(M,3,J,K)=-SPEED*COS(ANGLE)
        end DO
     end DO
  end DO
  !
  !     AVERAGE DATA WITHIN EACH VERTICAL CELL
  !
  IF (IOPT .EQ. 1) THEN
     DO K=1,NSTA
        NDATA=NREAD(K)
        DO J=1,NDATA
           DO JCELL=1,NCELL
              NCOUNT(JCELL,J,K)=0
              DO L=1,2
                 STATMP(JCELL,L,J,K)=0.
              end DO
              !
              DO M=1,NHTS(J,K)
                 IF (STADTA(M,1,J,K) .GE. CELLZB(JCELL) .AND. &
                      STADTA(M,1,J,K) .LE. CELLZB(JCELL+1)) GO TO 230
                 cycle
230              DO L=1,2
                    STATMP(JCELL,L,J,K)=STATMP(JCELL,L,J,K)+STADTA(M,L+1,J,K)
                 end DO
                 NCOUNT(JCELL,J,K)=NCOUNT(JCELL,J,K)+1
              end DO
              !
              IF (NCOUNT(JCELL,J,K) .EQ. 0) cycle
              !
              DO L=1,2
                 STATMP(JCELL,L,J,K)=STATMP(JCELL,L,J,K)/NCOUNT(JCELL,J,K)
              end DO
           end DO
        end DO
     end DO
     !
     !     INTERPOLATE FOR CELLS WITH NO DATA
     !
     DO K=1,NSTA
        NDATA=NREAD(K)
        DO J=1,NDATA
           DO M=1,NCELL
              IF (NCOUNT(M,J,K) .NE. 0) cycle
              !
              IF (M .EQ. 1 .OR. M .EQ. NCELL) THEN
                 DO L = 1,2
                    STATMP(M,L,J,K) = 999.
                 end DO
                 cycle
              ENDIF
              MM1 = M - 1
              MP1 = M + 1
              IF (NCOUNT(MM1,J,K) .NE. 0 .AND. NCOUNT(MP1,J,K) .NE. 0) THEN
                 DO L = 1,2
                    STATMP(M,L,J,K) = (STATMP(MM1,L,J,K) + STATMP(MP1,L,J,K))/2.
                 end DO
              ELSE
                 DO L=1,2
                    STATMP(M,L,J,K)=999.
                 end DO
              ENDIF
           end DO
        end DO
     end DO
  ENDIF
  !
  !     INTERPOLATE IN THE VERTICAL
  !
  IF (IOPT .EQ. 2) THEN
     !
     !     CALCULATE CELL CENTER HEIGHTS
     !
     DO M = 1,NCELL
        MP1 = M + 1
        DZ(M) = CELLZB(MP1) - CELLZB(M)
        CELLZC(M) = CELLZB(M) + 0.5 * DZ(M)
     end DO
     !
     !     LOCATE OBS ON EITHER SIDE OF CELL CENTER HEIGHT
     !
     DO K = 1,NSTA
        NDATA = NREAD(K)
        DO J = 1,NDATA
           DO JCELL = 1,NCELL
              NH = NHTS(J,K) - 1
              DO M = 1,NH
                 MP1 = M + 1
                 IF (STADTA(M,1,J,K) .LE. CELLZC(JCELL) .AND. &
                      STADTA(MP1,1,J,K) .GT. CELLZC(JCELL)) GO TO 430
              end DO
              !
              ! EXTEND DATA FOR LAYERS BELOW LOWEST OBS HEIGHT IF WITHIN CELL
              !
              IF (STADTA(1,1,J,K) .GT. CELLZC(JCELL)) THEN
                 ZDIF = STADTA(1,1,J,K) - CELLZC(JCELL)
                 HALF = 0.5 * DZ(JCELL)
                 IF (ZDIF .LT. HALF) THEN
                    DO L = 1,2
                       LP1 = L + 1
                       STATMP(JCELL,L,J,K) = STADTA(1,LP1,J,K)
                    end DO
                 ELSE
                    DO L = 1,2
                       STATMP(JCELL,L,J,K) = 999.
                    end DO
                 ENDIF
              ENDIF
              !
              ! EXTEND DATA FOR LAYERS ABOVE HIGHEST OBS HEIGHT IF WITHIN CELL
              !
              IF (STADTA(NHTS(J,K),1,J,K) .LT. CELLZC(JCELL)) THEN
                 ZDIF = CELLZC(JCELL) - STADTA(NHTS(J,K),1,J,K)
                 HALF = 0.5 * DZ(JCELL)
                 IF (ZDIF .LT. HALF) THEN
                    DO L = 1,2
                       LP1 = L + 1
                       STATMP(JCELL,L,J,K) = STADTA(NHTS(J,K),LP1,J,K)
                    end DO
                 ELSE
                    DO L = 1,2
                       STATMP(JCELL,L,J,K) = 999.
                    end DO
                 ENDIF
              ENDIF
              GO TO 460
              !
              !     INTERPOLATE LINEARLY TO CELL CENTER HEIGHT
              !
430           ZZ = CELLZC(JCELL) - STADTA(M,1,J,K)
              DELZ = STADTA(MP1,1,J,K) - STADTA(M,1,J,K)
              DELU = STADTA(MP1,2,J,K) - STADTA(M,2,J,K)
              DELV = STADTA(MP1,3,J,K) - STADTA(M,3,J,K)
              STATMP(JCELL,1,J,K) = STADTA(M,2,J,K) + (DELU/DELZ) * ZZ
              STATMP(JCELL,2,J,K) = STADTA(M,3,J,K) + (DELV/DELZ) * ZZ
460           CONTINUE
           end DO
        end DO
     end DO
  ENDIF
  !
  !     INTERPOLATE OBSERVATIONS IN TIME
  !
  DO K=1,NSTA
     NDATA=NREAD(K)
     DO J=NSTRHR,NENDHR
        JP1 = J + 1
        DO L=1,2
           DO M=1,NCELL
              WNDATA(M,L,JP1,K)=999.
           end DO
        end DO
        IF (NDATA .GT. 1) GO TO 310
        IF (ABS(HOUR(1,K)-FLOAT(J)) .GT. TDIF) cycle
        DO L=1,2
           DO M=1,NCELL
              WNDATA(M,L,JP1,K)=STATMP(M,L,1,K)
           end DO
        end DO
        cycle
        !
310     MHOUR=100*J
        XHOUR=J
        IF (MHOUR .GT. JHOUR(1,K)) GO TO 320
        IF (ABS(HOUR(1,K)-XHOUR) .GT. TDIF) cycle
        N=2
        GO TO 330
        !
320     DO N=2,NDATA
           IF (MHOUR .LT. JHOUR(N,K)) GO TO 350
        end DO
        IF (ABS(HOUR(NDATA,K)-XHOUR) .GT. TDIF) cycle
        N=NDATA+1
        GO TO 330
        !
350     HRDIFF=(XHOUR-HOUR(N-1,K))/(HOUR(N,K)-HOUR(N-1,K))
        IF ((ABS(HOUR(N-1,K)-XHOUR) .GT. TDIF) .AND. &
             (ABS(HOUR(N,K)-XHOUR) .GT. TDIF)) cycle
        IF (ABS(HOUR(N-1,K)-XHOUR) .GT. TDIF) THEN
           N=N+1
           GO TO 330
        ENDIF
        IF (ABS(HOUR(N,K)-XHOUR) .GT. TDIF) GO TO 330
        !
        !     DO NOT USE MISSING DATA IN TIME INTERPOLATION
        !
        DO L=1,2
           DO M=1,NCELL
              NSUB = 0
              IF (STATMP(M,L,N-1,K) .EQ. 999. .AND. STATMP(M,L,N,K) .EQ. 999.) cycle
              IF (STATMP(M,L,N-1,K) .EQ. 999.) THEN
                 NSUB = 1
                 N=N+1
                 HRDIFF=0.
              ENDIF
              IF (STATMP(M,L,N,K) .EQ. 999.) HRDIFF=0.
              WNDATA(M,L,JP1,K)=STATMP(M,L,N-1,K)+(STATMP(M,L,N,K)- &
                   STATMP(M,L,N-1,K))*HRDIFF
              !
              !     RESET N AND HRDIFF
              !
              IF (NSUB .EQ. 1) N = N - 1
              HRDIFF = (XHOUR - HOUR(N-1,K))/(HOUR(N,K) - HOUR(N-1,K))
           end DO
        end DO
        cycle
        !
330     DO L=1,2
           DO M=1,NCELL
              WNDATA(M,L,JP1,K)=STATMP(M,L,N-1,K)
           end DO
        end DO
     end DO

  end DO
  !
  !     WRITE OUT DATA FOR DIAGNO
  !
  DO K=1,NSTA
     WRITE(IFILE,2000) NAMST(K),UTMXST(K),UTMYST(K)
  end DO
  REL=1.0
  DO J=NSTRHR,NENDHR
     JP1 = J + 1
     DO K=1,NSTA
        WRITE(IFILE,2010) J,NAMST(K),REL,((WNDATA(M,L,JP1,K),L=1,2),M=1,NCELL)
     end DO
     WRITE(IFILE,2010) J,LAST
  end DO
  !
  ! 1010 FORMAT(10X,I5)
  ! 1020 FORMAT(10X,F5.1)
  ! 1030 FORMAT(10X,10F6.0)
1040 FORMAT(A4,1X,3F7.1)
1050 FORMAT(3I2,A4,1X,I4,1X,A3,50(I5))
1060 FORMAT('STAT=',A4,3X,'TIME=',I4,3X,'YEAR=',I2, &
          3X,'MONTH=',I2,3X,'DAY=',I2,3X,'KYEAR=',I2, &
          3X,'KMONTH=',I2,3X,'KDAY=',I2)
1070 FORMAT('TIME=',I4,1X,'FOR STATION=',A4,1X,'IS NOT BETWEEN',1X,I4,'AND',1X,I4)
1080 FORMAT('STATION=',A4,1X,'NOT FOUND')
1090 FORMAT(1X,'THIS IS NOT WIND DATA')
1100 FORMAT(1X,'FOR STATION=',A4,1X,'LEVEL=',I2,1X,'HEIGHT=',I4,1X,'IS NOT ACCEPTABLE')
1110 FORMAT(1X,'FOR STATION=',A4,1X,'LEVEL=',I2,1X,'DIRECTION=',I4,1X,'IS NOT ACCEPTABLE')
1120 FORMAT(1X,'FOR STATION=',A4,1X,'LEVEL=',I2,1X,'SPEED=',I4,1X,'IS NOT ACCEPTABLE')
1130 FORMAT(1X,'STATION=',A4,'HAS NO DATA')
1140 FORMAT('INTERPOLATION OPTION = 1: DATA ARE VERTICALLY AVERAGED WITHIN MODEL LAYERS')
1150 FORMAT('INTERPOLATION OPTION = 2: DATA ARE LINEARLY INTERPOLATED TO MODEL LEVELS')
2000 FORMAT('WIND STATION X & Y',2X,A4,2F8.1)
2010 FORMAT('UPPER WIND',I2,1X,A4,1X,31(F5.1))
999 call exit(0)
END PROGRAM PREUPR
!
!
!
SUBROUTINE JULDAY(JULIAN,KYR,KMO,KDAY,KODE)
  !
  !     CONVERTS JULIAN DATE TO DAY, MONTH, YEAR AND VICE-VERSA
  !
  !     KODE=1:  INPUT  - JULIAN,KYR
  !              OUTPUT - KMO,KDAY
  !
  !     KODE=2:  INPUT  - KYR,KMO,KDAY
  !              OUTPUT - JULIAN
  !
  DIMENSION NDAYMO(12)
  DATA NDAYMO/31,28,31,30,31,30,31,31,30,31,30,31/
  !
  if(kode == 1) then
     NDAYS=0
     DO I=1,12
        IM=I
        ND=NDAYMO(I)
        IF(I.EQ.2.AND.MOD(KYR,4).EQ.0) ND=29
        NDAYS=NDAYS+ND
        IF(NDAYS.GE.JULIAN) GO TO 250
     end DO
     write(*,*) ' ***  ERROR IN ROUTINE JULDAY'
     call exit(1)
250  CONTINUE
     KMO=IM
     KDAY=JULIAN-(NDAYS-ND)
  else
     NDAYS=0
     I2=KMO-1
     IF(I2.EQ.0) GO TO 450
     DO I=1,I2
        ND=NDAYMO(I)
        IF(I.EQ.2.AND.MOD(KYR,4).EQ.0) ND=29
        NDAYS=NDAYS+ND
     end DO
450  CONTINUE
     JULIAN=NDAYS+KDAY
  end if
END SUBROUTINE JULDAY

