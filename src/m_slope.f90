module m_slope

contains

  SUBROUTINE SLOPE(USLOPE,VSLOPE,HTOPO,TIME,HMAX)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  SLOPE    S.DOUGLAS, SAI
    !
    use m_domain
    implicit none

    real :: time
    real :: uslope(:,:,:), vslope(:,:,:)
    real :: htopo(:,:), hmax(:,:)

    real :: zero
    real :: aalpha, ang, delhi, delhj, delu, delv, dpr, dxi, dyi
    real :: htoim1, htoip1, htojm1, htojp1, rpd, sinalf, sinalt
    real :: thet, thetd, thetp, tmax, uvally
    integer :: i, it, j
    !
    !     THIS SUBROUTINE IS ADAPTED FROM THE 3-D COMPLEX TERRAIN WIND
    !     MODEL (YOCKE,1979).  THE U AND V WIND COMPONENTS DUE TO
    !     SLOPE FLOW ARE CALCULATED.
    !
    !     INPUT:  HTOPO (R ARRAY)  - GRIDDED TERRAIN HEIGHTS
    !             BETA2 (R ARRAY)  - FACTORS WHICH DETERMINE THE SIGN
    !                                OF THE SLOPE FLOW
    !             TINF (R)         - DOMAIN REPRESENTATIVE TEMPERATURE
    !             TIME (R)         - SIMULATION TIME
    !             HMAX (R ARRAY)   - MAXIMUM TERRAIN HEIGHTS WITHIN A
    !                                GIVEN RADIUS
    !             TGAMMA (R ARRAY) - TEMPERATURE LAPSE RATES (HOURLY )
    !
    !     OUTPUT:  USLOPE (R ARRAY) - U-COMPONENTS OF THE SLOPE FLOW
    !              VSLOPE (R ARRAY) - V-COMPONENTS OF THE SLOPE FLOW
    !
    DATA ZERO/0./
    !
    !     DEFINE CONVERSION FACTORS
    !
    RPD=0.017453
    DPR=57.296
    !
    !     INITIALIZE ARRAYS
    !
    uslope = zero
    vslope = zero
    !
    DO J=1,NY
       DO I=1,NX
          DELU=0.
          DELV=0.
          !
          !     CALCULATE THE TOPOGRAPHIC GRADIENTS
          !
          DXI=0.5/DX
          DYI=0.5/DY
          HTOIM1=HTOPO(I,J)
          HTOJM1=HTOPO(I,J)
          IF (I .GT. 1) HTOIM1=HTOPO(I-1,J)
          IF (J .GT. 1) HTOJM1=HTOPO(I,J-1)
          HTOIP1=HTOPO(I,J)
          HTOJP1=HTOPO(I,J)
          IF (I .LT. NX) HTOIP1=HTOPO(I+1,J)
          IF (J .LT. NY) HTOJP1=HTOPO(I,J+1)
          DELHI=(HTOIP1-HTOIM1)*DXI
          DELHJ=(HTOJP1-HTOJM1)*DYI
          !
          !     CALCULATE THE SLOPE FLOW
          !
          AALPHA=ATAN((DELHI**2+DELHJ**2)**0.5)
          IF (ABS(AALPHA) .LT. 0.009) cycle
          SINALF=SIN(ABS(AALPHA))
          SINALT=(DELHI**2+DELHJ**2)**0.5
          IF (SINALT .LT. 0.) SINALT=0.
          IT = IFIX(TIME/100.) + 1
          TMAX=HMAX(I,J)*TGAMMA(IT)
          UVALLY=BETA2(IT)*SQRT(1000.*ABS(TMAX)*SINALT/TINF)
          !
          !     SET UP DRAINAGE VECTOR DIRECTIONS
          !
          IF (DELHI .EQ. 0.) THEN
             IF (DELHJ .EQ. 0.) cycle
             IF (DELHJ .LT. 0.) THET=270.
             IF (DELHJ .GT. 0.) THET=90.
          ELSE
             THETP=ATAN(DELHJ/DELHI)*DPR
             IF (DELHI .LT. 0.) THET=THETP+180.
             IF (DELHI .GT. 0.) THEN
                IF (DELHJ .GT. 0.) THEN
                   THET=THETP
                ELSE
                   THET=THETP+360.
                ENDIF
             ENDIF
          ENDIF
          IF (THET .GE. 0. .AND. THET .LE. 90.) THETD=90.-THET
          IF (THET .GT. 90. .AND. THET .LE. 360.) THETD=450.-THET
          !
          !     RESOLVE SLOPE FLOW INTO COMPONENTS
          !
          ANG=(270.-THETD)*RPD
          DELU=-COS(ANG)*UVALLY
          DELV=-SIN(ANG)*UVALLY
          !
          !     SET UP USLOPE AND VSLOPE ARRAYS
          !
          USLOPE(I,J,1)=DELU
          VSLOPE(I,J,1)=DELV
          !     USLOPE(I,J,2)=0.5*DELU
          !     VSLOPE(I,J,2)=0.5*DELV
          !     USLOPE(I,J,3)=-0.5*DELU
          !     VSLOPE(I,J,3)=-0.5*DELV
       end DO
    end DO
    !
    !     PRINT OUT SLOPE WIND COMPONENTS
    !
    !     DO 110 J=1,NY
    !     DO 110 I=1,NX
    !     USFC(I,J)=USLOPE(I,J,1)
    !     VSFC(I,J)=VSLOPE(I,J,1)
    ! 110 CONTINUE
    !
    !     WRITE(IWR,1010)
    !     WRITE(IWR,1020)
    !     CALL WNDLPT(USFC)
    !     WRITE(IWR,1030)
    !     WRITE(IWR,1020)
    !     CALL WNDLPT(VSFC)
    !
    ! 1010 FORMAT(/,5X,'X-COMPONENT OF SLOPE WIND (U) AT THE SURFACE')
    ! 1020 FORMAT(5X,44('-'))
    ! 1030 FORMAT(/,5X,'Y-COMPONENT OF SLOPE WIND (V) AT THE SURFACE')
    RETURN
  END SUBROUTINE SLOPE

end module m_slope
