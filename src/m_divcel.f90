module m_divcel

contains

  SUBROUTINE DIVCEL(U,V,W,DIV,UB,VB,DIVMAX,K)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 870717  WINDBC
    !
    use m_domain

    real :: U(:,:,:),V(:,:,:)
    real :: W(:,:,:),DIV(:,:,:)
    real :: UB(:,:,:),VB(:,:,:)
    !
    !     COMPUTES THE 3-D DIVERGENCE IN EACH GRID CELL
    !
    !     INPUTS:  U (R ARRAY)  - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY)  - GRIDDED Y-DIRECTION WIND COMPONENTS
    !              W (R ARRAY)  - GRIDDED VERTICAL WIND COMPONENTS
    !              UB (R ARRAY) - U-COMPONENT BOUNDARY VALUES
    !              VB (R ARRAY) - V-COMPONENT BOUNDARY VALUES
    !
    !     OUTPUTS:  DIV (R ARRAY) - GRIDDED 3-D DIVERGENCE
    !

    HTOP=1.0
    DXI=1.0/(2.0*DX)
    DYI=1.0/(2.0*DY)
    DZI=1.0/DZ(K)

    DO J=1,NY
       DO I=1,NX
          DIV(I,J,K)=0.
          WPH=W(I,J,K+1)
          WMH=W(I,J,K)
          UPH=UB(J,2,K)
          IF(I.LT.NX) UPH=U(I+1,J,K)
          VPH=VB(I,2,K)
          IF(J.LT.NY) VPH=V(I,J+1,K)
          UMH=UB(J,1,K)
          IF(I.GT.1) UMH=U(I-1,J,K)
          VMH=VB(I,1,K)
          IF(J.GT.1) VMH=V(I,J-1,K)
          !
          !     DIVERGENCE IS EVALUATED USING CENTRAL DIFFERENCES
          !
          DUDX=(UPH-UMH)*DXI
          DVDY=(VPH-VMH)*DYI
          DWDZ=(WPH-WMH)*DZI
          DIV(I,J,K)=HTOP*(DUDX+DVDY)+DWDZ
          DIVABS=ABS(DIV(I,J,K))
          DIVMAX=MAX(DIVABS,DIVMAX)
       end do
    end do
    !
    !     SCALE DIVERGENCE FOR TERRAIN-CONFORMAL SYSTEM
    !
    DIVMAX=DIVMAX/HTOP
    RETURN
  END SUBROUTINE DIVCEL

end module m_divcel
