module m_barier
  use m_domain

contains

  SUBROUTINE BARIER(X,Y,XS,YS,OK)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  BARIER
    !
    real :: A(3), B(3), C(3)
    !
    !     THIS ROUTINE CALCULATES WHAT SIDE OF A BARRIER A POINT IS ON
    !     BARRIERS ARE FINITE LENGTH LINE SEGMENTS
    !
    !     INPUTS:  BARXY(1,I) (R) - UTM-X COORDINATE OF FIRST END POINT
    !              BARXY(2,I) (R) - UTM-Y COORDINATE OF FIRST END POINT
    !              BARXY(3,I) (R) - UTM-X COORDINATE OF SECOND END POINT
    !              BARXY(4,I) (R) - UTM-Y COORDINATE OF SECOND END POINT
    !              NUMBAR (I)   - NUMBER OF BARRIERS (UP TO 20)
    !              IFIN  (I)    - IF 0 SET UP PT SLOPE LINES
    !              X (R)        - X-COORDINATE OF POINT OF INTEREST
    !              Y (R)        - Y-COORDINATE OF POINT OF INTEREST
    !              XS (R)       - X-COORDINATE OF REFERENCE POINT
    !              YS (R)       - Y-COORDINATE OF REFERENCE POINT
    !
    !     OUTPUTS:  SLPIN(1,I) (R) - SLOPE OF LINE
    !               SLPIN(2,I) (R) - INTERCEPT OF LINE
    !               OK (R)         - FLAG IF POS (X-Y) IS ON SAME SIDE A
    !                                (XS-YS)
    OK = 1.0
    IF (NUMBAR .LE. 0) return
    !
    !     SETUP SLOPE INTERCEPT FORM OF A LINE
    !
    !
    IF (IFIN .le. 0) then
       DO I = 1,NUMBAR
          DY = BARXY(2,I)-BARXY(4,I)
          DX = BARXY(1,I)-BARXY(3,I)
          SLPIN(1,I) = DY/DX
          SLPIN(2,I) = BARXY(2,I)-SLPIN(1,I)*BARXY(1,I)
       end DO
       IFIN = 1
    end IF

    C(1) = XS-X
    C(2) = YS-Y
    A(3) = 0.
    B(3) = 0.
    C(3) = 0.
    DO I = 1,NUMBAR
       D1 = YS-SLPIN(1,I)*XS-SLPIN(2,I)
       D2 = Y-SLPIN(1,I)*X-SLPIN(2,I)
       IF (D1*D2.GE.0.) cycle
       A(1) = BARXY(1,I)-X
       A(2) = BARXY(2,I)-Y
       B(1) = BARXY(3,I)-X
       B(2) = BARXY(4,I)-Y
       COSAB = UNIDOT(A,B)
       COSAC = UNIDOT(A,C)
       COSBC = UNIDOT(B,C)
       IF (COSAC .LT. COSAB .OR. COSBC .LT. COSAB) cycle
       OK = -1.0
       exit
    end DO
    !
    RETURN
  END SUBROUTINE BARIER

end module m_barier
