module m_wndlpt

contains

  SUBROUTINE WNDLPT(GRID)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  WNDLPT
    !
    use m_domain

    CHARACTER(len=1) :: V
    CHARACTER(len=3) :: H(180)
    real :: GRID(:,:)

    integer :: MAP(NX,NY)
    !
    !     CREATES A NUMBER MAP OF THE ARRAY 'GRID' SCALED BY FACTOR
    !
    !     INPUTS:  GRID (R ARRAY) - GRIDDED ARRAY OF SOME VARIABLE
    !
    DATA V/'I'/, ICCM /1/
    DATA H /180*'--+'/
    VMAX = 0.0
    DO J=1,NY
       DO I=1,NX
          VAL = ABS(GRID(I,J))
          VMAX = MAX(VAL,VMAX)
       end do
    end do
    IF(VMAX.EQ.0.) WRITE(IWR,995)
    IF(VMAX.EQ.0.) RETURN
    IF(VMAX.LT.10.)  GO TO 30
    IF(VMAX.GE.100.) GO TO 40
    FACTOR = 1.00
    GO TO 50
30  do i = 1,20
       factor = 10.**(i)
       ifx = ifix(factor*vmax)
       if(ifx.ge.10 )  go to 50
    end do
    RETURN
40  DO I = 1,20
       FACTOR = 10.**(-I)
       IFX = IFIX(FACTOR*VMAX)
       IF(IFX.LT.100 )   GO TO 50
    end DO
    RETURN
50  CONTINUE
    DO I=1,NX
       DO J=1,NY
          VAL = GRID(I,J)*FACTOR
          IF(VAL.GT.0.)   MAP(I,J) = IFIX( VAL + 0.50)
          IF(VAL.LT.0.)   MAP(I,J) = IFIX( VAL - 0.50)
          IF(VAL.EQ.0.)   MAP(I,J) = 0
       end do
    end do
    IL = 1
    IR = MIN0(NX,36)
    IR = MAX0(IR,1)
    JT = 1
    JB = MIN0(NY,150)
    JB = MAX0(JB,1)
    IRL = IR*ICCM
    WRITE(IWR,1000) (I,I=IL,IRL,ICCM)
    WRITE(IWR,1002) V, (H(I),I=IL,IR)
    DO J=JT,JB
       K = JB - J + JT
       JRM = (K-1)*ICCM + 1
       WRITE(IWR,1003) JRM,V,(MAP(I,K),I=IL,IR)
    end do
    IF (NX .LE. 36) GO TO 80
    IL=37
    IR=MIN0(NX,72)
    JT=1
    JB=MIN0(NY,150)
    JB=MAX0(JB,1)
    IRL=IR*ICCM
    WRITE(IWR,1000)(I,I=IL,IRL,ICCM)
    WRITE(IWR,1002) V,(H(I),I=IL,IR)
    DO J=JT,JB
       K=JB-J+JT
       JRM=(K-1)*ICCM+1
       WRITE(IWR,1003) JRM,V,(MAP(I,K),I=IL,IR)
    end do
    IF (NX .LE. 72) GO TO 80
    IL=73
    IR=MIN0(NX,108)
    JT=1
    JB=MIN0(NY,150)
    JB=MAX0(JB,1)
    IRL=IR*ICCM
    WRITE(IWR,1000)(I,I=IL,IRL,ICCM)
    WRITE(IWR,1002) V,(H(I),I=IL,IR)
    DO J=JT,JB
       K=JB-J+JT
       JRM=(K-1)*ICCM+1
       WRITE(IWR,1003) JRM,V,(MAP(I,K),I=IL,IR)
    end do
    IF (NX .LE. 108) GO TO 80
    IL=109
    IR=MIN0(NX,144)
    JT=1
    JB=MIN0(NY,150)
    JB=MAX0(JB,1)
    IRL=IR*ICCM
    WRITE(IWR,1000)(I,I=IL,IRL,ICCM)
    WRITE(IWR,1002) V,(H(I),I=IL,IR)
    DO J=JT,JB
       K=JB-J+JT
       JRM=(K-1)*ICCM+1
       WRITE(IWR,1003) JRM,V,(MAP(I,K),I=IL,IR)
    end do
    IF (NX .LE. 144) GO TO 80
    IL=145
    IR=MIN0(NX,180)
    JT=1
    JB=MIN0(NY,150)
    JB=MAX0(JB,1)
    IRL=IR*ICCM
    WRITE(IWR,1000)(I,I=IL,IRL,ICCM)
    WRITE(IWR,1002) V,(H(I),I=IL,IR)
    DO J=JT,JB
       K=JB-J+JT
       JRM=(K-1)*ICCM+1
       WRITE(IWR,1003) JRM,V,(MAP(I,K),I=IL,IR)
    end DO
80  CONTINUE
    WRITE(IWR,1004) FACTOR
    RETURN
995 FORMAT(/,9X,'FIELD CONTAINS ALL ZEROS - PRINTING SUPPRESSED')
1000 FORMAT(/,7X,38I3)
1002 FORMAT(6X,A1,38A3)
1003 FORMAT(3X,I3,1X,A1,38I3)
1004 FORMAT(/,9X,'ARRAY HAS BEEN SCALED BY',E9.1,'  FOR PRINTING')
  END SUBROUTINE WNDLPT

end module m_wndlpt
