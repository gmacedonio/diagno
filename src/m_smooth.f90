module m_smooth

contains

  SUBROUTINE SMOOTH(U,V,UB,VB)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  SMOOTH    S. DOUGLAS, SAI
    !
    use m_domain
    implicit none
    integer :: i, im1, ip1, j, jm1, jp1, k, n
    real :: uim1, uip1, ujm1, ujp1, vim1, vip1, vjm1, vjp1

    real :: U(:,:,:), V(:,:,:), UB(:,:,:), VB(:,:,:)
    !
    !     APPLIES SMOOTHING TO INTERPOLATED FILEDS
    !
    !    INPUTS:  U (R ARRAY)  - GRIDDED X-DIRECTION WIND COMPONENTS
    !             V (R ARRAY)  - GRIDDED Y-DIRECTION WIND COMPONENTS
    !             UB (R ARRAY) - U-COMPONENT BOUNDARY VALUES
    !             VB (R ARRAY) - V-COMPONENT BOUNDARY VALUES
    !             NSMTH (I)    - NUMBER OF SMOOTHING PASSES FOR ALL
    !                            LAYERS EXCEPT THE SURFACE LAYER
    !
    !     OUTPUTS:  U (R ARRAY)  - SMOOTHED X-DIRECTION WIND COMPONENTS
    !               V (R ARRAY)  - SMOOTHED Y-DIRECTION WIND COMPONENTS
    !
    DO K=1,NZ
       DO N=1,NSMTH
          !
          !     SMOOTH SURFACE LAYER ONLY TWICE
          !
          IF (K .EQ. 1 .AND. N .GT. 2) cycle
          DO J=1,NY
             DO I=1,NX
                IP1=I+1
                JP1=J+1
                IM1=I-1
                JM1=J-1
                UIM1=UB(J,1,K)
                IF (I .GT. 1) UIM1=U(IM1,J,K)
                UJM1=U(I,J,K)
                IF (J .GT. 1) UJM1=U(I,JM1,K)
                VIM1=V(I,J,K)
                IF (I .GT. 1) VIM1=V(IM1,J,K)
                VJM1=VB(I,1,K)
                IF (J .GT. 1) VJM1=V(I,JM1,K)
                UIP1=UB(J,2,K)
                IF (I .LT. NX) UIP1=U(IP1,J,K)
                UJP1=U(I,J,K)
                IF (J .LT. NY) UJP1=U(I,JP1,K)
                VIP1=V(I,J,K)
                IF (I .LT.NX) VIP1=V(IP1,J,K)
                VJP1=VB(I,2,K)
                IF (J .LT. NY) VJP1=V(I,JP1,K)
                !
                U(I,J,K)=0.5*U(I,J,K)+0.125*(UIP1+UIM1+UJP1+UJM1)
                V(I,J,K)=0.5*V(I,J,K)+0.125*(VIP1+VIM1+VJP1+VJM1)
             end do
          end do
       end do
    end DO
    RETURN
  END SUBROUTINE SMOOTH

end module m_smooth
