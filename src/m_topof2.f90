module m_topof2

contains

  SUBROUTINE TOPOF2(U,V,W,HTOPO,TIME)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  TOPOF2    S.DOUGLAS, SAI
    !
    use m_domain
    implicit none

    real :: time
    real :: U(:,:,:), V(:,:,:), W(:,:,:), HTOPO(:,:)

    integer :: i, it, j, k, kp1
    real :: bk, bkz, delhi, delhj, dwdz, dwdz1, dxi, dyi, dzi, hinv
    real :: htoim1, htoip1, htojm1, htojp1, s, tau, tgamma2, wtopo
    real :: umod
    real :: W1(NZ+1)
    !
    !     THIS SUBROUTINE IS ADAPTED FROM THE 3-D COMPLEX TERRAIN
    !     WIND MODEL (YOCKE,1979).  THE VERTICAL VELOCITY DUE TO
    !     TOPOGRAPHIC EFFECTS IS COMPUTED AND TRANSFORMED TO TERRAIN
    !     FOLLOWING COORDINATES
    !
    !
    !     INPUT: U (R ARRAY) - GRIDDED X-DIRECTION WIND COMPONENTS
    !     V (R ARRAY)        - GRIDDED Y-DIRECTION WIND COMPONENTS
    !     HTOPO (R ARRAY)    - GRIDDED TERRAIN HEIGHTS
    !     CELLZB (R ARRAY)   - VERTICAL LEVEL HEIGHTS
    !     TGAMMA (R ARRAY)   - TEMPERATURE LAPSE RATES (HOURLY)
    !     TINF (R)           - DOMAIN REPRESENTATIVE TEMPERATURE
    !     ALPHA (R)          -  EMPIRICAL COEFFICIENT FOR
    !     TOPOGRAPHIC EFFECTS
    !     TIME (R)         - SIMULATION TIME
    !
    !     OUTPUT:  W (R ARRAY) - TERRAIN INDUCED VERTICAL VELOCITY
    !
    !     CALCULATE THE STABILITY PARAMETER, S
    !
    tau=-0.01
    it = ifix(time/100.) + 1
    tgamma2 = tgamma(it)-tau
    if (tgamma2 .lt. 0.) s=-1.0
    if (tgamma2 .ge. 0.) s=(9.8*tgamma2/tinf)**0.5
    !
    !     CALCULATE THE VERTICAL VELOCITY
    !
    do j=1,ny
       do i=1,nx
          !
          !     CALCULATE THE WAVENUMBER OF THE ATMOSPHERE
          !
          hinv = 500.
          if (s .le. 0.) then
             bk=2./hinv
          else
             umod = sqrt(u(i,j,nz)**2+v(i,j,nz)**2)
             if(umod > 1e-6) then
                bk = s/umod
             else
                bk = 100.0
             end if
          endif
          !
          !     CALCULATE THE TOPOGRAPHIC GRADIENTS
          !
          dxi=0.5/dx
          dyi=0.5/dy
          htoim1=htopo(i,j)
          htojm1=htopo(i,j)
          if (i .gt. 1) htoim1=htopo(i-1,j)
          if (j .gt. 1) htojm1=htopo(i,j-1)
          htoip1=htopo(i,j)
          htojp1=htopo(i,j)
          if (i .lt. nx) htoip1=htopo(i+1,j)
          if (j .lt. ny) htojp1=htopo(i,j+1)
          delhi=(htoip1-htoim1)*dxi
          delhj=(htojp1-htojm1)*dyi
          !
          !     CALCULATE THE VERTICAL VELOCITY DUE TO TOPOGRAPHIC EFFECTS
          !
          wtopo=u(i,j,1)*delhi+v(i,j,1)*delhj
          !
          !     SET THE LOWER BOUNDARY CONDITION
          !
          w1(1)=wtopo
          w(i,j,1)=wtopo
          !
          !     COMPUTE THE EXPONENTIAL DECAY OF VERTICAL VELOCITY
          !
          do k=1,nz
             kp1=k+1
             bkz=bk*cellzb(kp1)
             if (bkz .gt. 30.) bkz=30.
             w1(kp1)=wtopo*exp(-bkz)
             !
             !     CALCULATE DWDZ
             !
             dzi=1.0/dz(k)
             dwdz1=(w1(kp1)-w1(k))*dzi
             dwdz=alpha*dwdz1
             !
             !     CALCULATE W FROM DWDZ
             !
             w(i,j,kp1)=dwdz/dzi+w(i,j,k)
          end do
          !
          !
          !     TRANSFORM TO TERRAIN FOLLOWING COORDINATES
          !
          W(I,J,1)=W(I,J,1)-U(I,J,1)*DELHI-V(I,J,1)*DELHJ
          do k=1,nz
             kp1=k+1
             w(i,j,kp1)=w(i,j,kp1)-u(i,j,k)*delhi-v(i,j,k)*delhj
          end do
       end do
    end do
    return
  end SUBROUTINE TOPOF2

end module m_topof2
