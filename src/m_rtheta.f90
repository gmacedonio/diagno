module m_rtheta

contains

  SUBROUTINE RTHETA(U,V,ARRAY)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  RTHETA
    !
    use m_domain
    use m_wndlpt

    real :: U(:,:,:), V(:,:,:), ARRAY(:,:,:)
    !
    !     RTHETA PRINTS WIND FIELD SPEED AND DIRECTION
    !
    !     INPUTS:  U (R ARRAY) - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY) - GRIDDED Y-DIRECTION WIND COMPONENTS
    !
    !     CONVERSION FACTOR FROM RADIANS TO DEGREES
    !
    FACTOR=180./3.141592654
    DO K=1,NZPRNT
       DO J=1,NY
          DO I=1,NX
             UU=U(I,J,K)
             VV=V(I,J,K)
             SUM =  UU**2 + VV**2
             ARRAY(I,J,1)=SQRT(SUM)
             ARRAY(I,J,2)=0.
             IF(SUM.LT.1.0E-10) cycle
             ANGLE=270.-ATAN2(VV,UU)*FACTOR
             ARRAY(I,J,2)=AMOD(ANGLE,360.)
          end DO
       end DO

       WRITE(IWR,60) K
       WRITE(IWR,61)
       CALL WNDLPT(ARRAY(1:nx,1:ny,1))
       WRITE(IWR,70) K
       WRITE(IWR,71)
       CALL WNDLPT(ARRAY(1:nx,1:ny,2))
    end DO
    RETURN
60  FORMAT(//,5X,'WIND SPEED (M/S) AT LEVEL = ',I4)
61  FORMAT(5X,26('-'))
70  FORMAT(//,5X,'WIND DIRECTION (DEGREES) AT LEVEL = ',I4)
71  FORMAT(5X,34('-'))
  END SUBROUTINE RTHETA

end module m_rtheta
