module m_minim

contains

  SUBROUTINE MINIM(U,V,W,UB,VB,DIV)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  MINIM    S.DOUGLAS, SAI
    !
    use m_domain
    use m_divcel
    use m_windbc

    real :: U(:,:,:), V(:,:,:), W(:,:,:)
    real :: UB(:,:,:), VB(:,:,:), DIV(:,:,:)
    !
    !     ITERATIVE SCHEME TO MINIMIZE DIVERGENCE
    !
    !     INPUTS:  U (R ARRAY)   - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY)   - GRIDDED Y-DIRECTION WIND COMPONENTS
    !              W (R ARRAY)   - GRIDDED VERTICAL WIND COMPONENTS
    !              UB (R ARRAY)  - U-COMPONENT BOUNDARY VALUES
    !              VB (R ARRAY)  - V-COMPONENT BOUNDARY VALUES
    !              DIV (R ARRAY) - 3-D DIVERGENCE
    !              NITER (I)     - MAXIMUM NUMBER OF ITERATIONS
    !              DIVLIM (R)    - MAXIMUM DIVERGENCE
    !
    !
    !     OUTPUTS:  U (R ARRAY) - NON-DIVERGENT X-DIRECTION WIND
    !                             COMPONENT
    !               V (R ARRAY) - NON-DIVERGENT Y-DIRECTION WIND
    !                             COMPONENT
    WRITE(IWR,2809)
    DO K=1,NZ
       ITER=0
       !
       !     COMPUTE DIVERGENCE
       !
       DIVMAX=-1.0E+09
       CALL DIVCEL(U,V,W,DIV,UB,VB,DIVMAX,K)
       IF (DIVMAX .LE. DIVLIM) cycle
       !
       !     ADJUST HORIZONTAL WIND FIELDS
       !
       DO ITER=1,NITER
          DO IDIR=1,4
             DO JJ=1,NY
                DO II=1,NX
                   !      GO TO (50,60,70,80), IDIR
                   select case (idir)
                   case (1)
                      I=II
                      J=JJ
                   case (2)
                      I=NX-II+1
                      J=JJ
                   case (3)
                      I=II
                      J=NY-JJ+1
                   case (4)
                      I=NX-II+1
                      J=NY-JJ+1
                   end select
                   IF (DIV(I,J,K) .EQ. 0.) cycle
                   IP1=I+1
                   IM1=I-1
                   JP1=J+1
                   JM1=J-1
                   UIM1=UB(J,1,K)
                   IF (I .GT. 1) UIM1=U(IM1,J,K)
                   VJM1=VB(I,1,K)
                   IF (J .GT. 1) VJM1=V(I,JM1,K)
                   UIP1=UB(J,2,K)
                   IF (I .LT. NX) UIP1=U(IP1,J,K)
                   VJP1=VB(I,2,K)
                   IF (J .LT. NY) VJP1=V(I,JP1,K)
                   ALPHA1=0.5
                   ALPHA2=0.5
                   ALPHA3=0.5
                   ALPHA4=0.5
                   AL1234=ALPHA1+ALPHA2+ALPHA3+ALPHA4
                   IF (AL1234 .LT. 1.E-6) cycle
                   UT=-2.*(DIV(I,J,K)*DX)/AL1234
                   VT=-2.*(DIV(I,J,K)*DY)/AL1234
                   UIP1=UIP1+ALPHA1*UT
                   UIM1=UIM1-ALPHA2*UT
                   VJP1=VJP1+ALPHA3*VT
                   VJM1=VJM1-ALPHA4*VT
                   IF (I .GT. 1) U(IM1,J,K)=UIM1
                   IF (I .LT. NX) U(IP1,J,K)=UIP1
                   IF (J .GT. 1) V(I,JM1,K)=VJM1
                   IF (J .LT. NY) V(I,JP1,K)=VJP1
                end DO
             end DO
             DIVMAX=-1.0E+09
             CALL DIVCEL(U,V,W,DIV,UB,VB,DIVMAX,K)
             !
             !     RESET BOUNDARY CONDITIONS
             !
             CALL WINDBC(U,V,UB,VB,K)
          end DO
          !
          !     CONVERGENCE TEST FOR DIVERGENCE MAGNITUDE
          !
          IF (DIVMAX .LE. DIVLIM) exit
       end DO
       WRITE(IWR,2810) K,ITER,DIVMAX
    end DO
    !
2809 FORMAT(//,5X,'SUMMARY OF DIVERGENCE MINIMIZATION' &
         ,//,'  LEVEL   ITERATIONS   MAXIMUM DIVERGENCE (/SEC)')
2810 FORMAT(3X,I2,I11,12X,E10.3)
    RETURN
  END SUBROUTINE MINIM

end module m_minim
