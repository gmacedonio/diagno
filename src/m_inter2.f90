module m_inter2

contains

  SUBROUTINE INTER2(STDAT1,NST,XST,YST,RS,IS,WORK,FIELD1,IFLAG,FGFLD,LNDWTR)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  INTER2    S.DOUGLAS, SAI
    !
    use m_domain
    use m_barier
    implicit none

    integer :: nst, iflag
    integer :: is(:)
    real :: xst(:), yst(:), rs(:), work(:)
    real :: stdat1(:,:)
    real :: field1(:,:,:), fgfld(:,:,:)
    integer :: lndwtr(:,:)
    !
    integer :: i, j, k, l, m, n, nk, nmin, nn
    integer :: js(maxval(nintrp))
    real :: fmin, ok
    real :: r, rmax, rsx, rsy, wt, x, y, zero

    real :: field2(nx,ny,nz)

    !
    !     COMPUTE 3-D WIND FIELD FROM STATION DATA
    !     BY THE ERT INTERPOLATION SCHEME
    !
    !     INPUTS:  STDAT1 (R ARRAY) - WIND COMPONENT STATION DATA
    !              NST (I)          - NUMBER OF STATIONS
    !              XST (R ARRAY)    - LOCAL X-COORDINATES OF STATIONS
    !              YST (R ARRAY)    - LOCAL Y-COORDINATES OF STATIONS
    !              RMIN (R)         - MINIMUM RADIUS OF INFLUENCE
    !              RMAX1 (R)        - MAXIMUM RADIUS OF INFLUENCE OVER
    !                                 LAND IN THE SURFACE LAYER
    !              RMAX2 (R)        - MAXIMUM RADIUS OF INFLUENCE OVER
    !                                 LAND IN UPPER LAYERS
    !              RMAX3 (R)        - MAXIMUM RADIUS OF INFLUENCE OVER
    !                                 WATER
    !              R1 (R)           - WEIGHTING PARAMETER FOR SURFACE
    !                                 LAYER
    !              R2 (R)           - WEIGHTING PARAMETER FOR UPPER
    !                                 LAYERS
    !            FGFLD (R ARRAY)     - DIAGNOSTIC WIND FIELD
    !            LNDWTR (I ARRAY)    - GRIDDED SURFACE TYPE INDICATORS
    !
    !     OUTPUTS:  FIELD1 (R ARRAY) - INTERPOLATED WIND COMPONENT
    !                                  ARRAY
    !
    DATA ZERO/0./

    !
    !     INITIALIZE FIELD2 ARRAY
    !
    field2 = 0.  ! Clear
    !
    !     CONVERT GRID SPACING TO KM
    !
    DXK=DX*0.001
    DYK=DY*0.001
    !
    !     DETERMINE RMAX
    !
    RMAX = MAX(RMAX1,RMAX2,RMAX3)
    !
    DO J=1,NY
       DO I=1,NX
          X = (I-0.5)*DXK
          Y = (J-0.5)*DYK
          RS(1:NST) = EDIT
          DO L=1,NST
             IF(IFLAG.LE.0) GO TO 90
             !
             !     CHECK FOR BARRIERS TO INTERPOLATION
             !
             CALL BARIER(X,Y,XST(L),YST(L),OK)
             IF(OK.LE.0.) GO TO 115
             !
             !     SKIP STATION IF NO DATA AT ANY ELEVATION
             !
90           DO K=1,NZ
                IF(STDAT1(K,L).LT.EDITL) GO TO 110
             end DO
             GO TO 115
             !
             !     COMPUTE DISTANCE TO STATIONS LESS THAN RMAX AWAY
             !
110          RSX = X - XST(L)
             RSY = Y - YST(L)
             RS(L) = RSX**2 + RSY**2
             RS(L) = SQRT(RS(L))
             IF(RS(L).LT.RMIN) RS(L)=RMIN
             IF(RS(L) .LE. RMAX)  GO TO 115
             RS(L) = EDIT
115          CONTINUE
          end DO
          !
          !     ORDER STATIONS
          !
          DO NN = 1,NST
             IS(NN) = IEDIT
          end DO
          ! CALL XMIT(NST,RS,WORK)
          work(1:nst) = rs(1:nst)
          N = 0
          DO L = 1,NST
             CALL FMINF(WORK,NST,FMIN,NMIN)
             WORK(NMIN) = EDIT + L
             IS(L) = NMIN
             IF(FMIN.GT.EDITL) exit
             N = N + 1
          end DO
          DO K=1,NZ
             !
             !     FIND STATIONS WITH DATA FOR THIS LEVEL
             !
             IF (K .EQ. 1 ) THEN
                R = R1
             ELSE
                R = R2
             ENDIF
             NK = 0
             DO M=1,N
                L=IS(M)
                IF(STDAT1(K,L).GT.EDITL) cycle
                IF (K .EQ. 1) THEN
                   IF (RS(L) .GT. RMAX1 .AND. LNDWTR(I,J) .EQ. 1) cycle
                ENDIF
                IF (K .GT. 1) THEN
                   IF (RS(L) .GT. RMAX2 .AND. LNDWTR(I,J) .EQ. 1) cycle
                ENDIF
                NK = NK + 1
                JS(NK) = IS(M)
                IF(NK.EQ.NINTRP(K)) GO TO 165
             end DO
             IF (NK .GT. 0 .OR. LNDWTR(I,J) .EQ. 1) GO TO 165
             WRITE(IWR,2065) I,J,K
             call exit(1)
165          CONTINUE
             !
             !     FIND THE INTERPOLATION WEIGHTING FACTOR FOR EACH VALID STATION
             !
             DO M = 1,NK
                L=JS(M)
                IF(STDAT1(K,L) .GT. EDITL) cycle
                WT = 1./(RS(L)**2)
                !
                !     APPLY WEIGHTING FACTOR
                !
                FIELD1(I,J,K) = FIELD1(I,J,K) + STDAT1(K,L)*WT
                FIELD2(I,J,K) = FIELD2(I,J,K) + WT
             end DO
             IF (LNDWTR(I,J) .EQ. 1) THEN
                FIELD1(I,J,K) = (FIELD1(I,J,K) + FGFLD(I,J,K)/R**2)/ &
                     (FIELD2(I,J,K) + 1./R**2)
             ELSE
                FIELD1(I,J,K) = FIELD1(I,J,K)/FIELD2(I,J,K)
             ENDIF
          end DO
       end DO
    end DO
    RETURN
    !2064 FORMAT('  JOB ABORTED  --  THERE ARE NO CLOSE STATIONS FOR', &
    !          ' I = ',I4,' J = ',I4,' IN SUBROUTINE INTER2')
2065 FORMAT('  JOB ABORTED  --  THERE ARE NO CLOSE STATIONS FOR', &
         ' I = ',I4,' J = ',I4,' K = ',I4,' IN SUBROUTINE INTER2')
  END SUBROUTINE INTER2

end module m_inter2
