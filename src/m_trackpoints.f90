!> Module for managing tracking points
module m_trackpoints
  !
  ! Date: 9-NOV-2023
  !
  ! Provides:
  ! subroutine tracking_point_clear(tp)
  ! subroutine tracking_points_load(tp, fitrack)
  !
  use m_domain
  use m_interp
  use m_angles
  implicit none

  private :: get_free_unit

  ! Tracking point
  type :: tpoint
     integer :: ntime = 0   ! Number of stored times
     real :: x, y, z
     real :: xwgt, ywgt, zwgt
     integer :: xind, yind, zind
     character(len=10) :: label
     real, allocatable :: time(:)
     real, allocatable :: u(:), v(:), w(:)   ! Values at different times
   contains
     procedure :: memalloc => tpoint_memalloc
     procedure :: free     => tpoint_free
  end type tpoint

  ! The list of tracking points
  type :: trackpoints
     integer :: np = 0   ! Number of tracking points
     type(tpoint), allocatable :: p(:)
   contains
     procedure :: clear    => trackpoints_clear
     procedure :: memalloc => trackpoints_memalloc
     procedure :: addtime  => trackpoints_addtime
     procedure :: load     => trackpoints_load
     procedure :: print    => trackpoints_print
  end type trackpoints

contains

  !> Allocate memory
  subroutine tpoint_memalloc(tp, ntime)
    implicit none
    class(tpoint) :: tp
    integer :: ntime
    call tp%free
    allocate(tp%time(ntime))
    allocate(tp%u(ntime))
    allocate(tp%v(ntime))
    allocate(tp%w(ntime))
  end subroutine tpoint_memalloc

  !> Free memory
  subroutine tpoint_free(tp)
    implicit none
    class(tpoint) :: tp
    if(allocated(tp%time)) deallocate(tp%time)
    if(allocated(tp%u)) deallocate(tp%u)
    if(allocated(tp%v)) deallocate(tp%v)
    if(allocated(tp%w)) deallocate(tp%w)
    tp%ntime = 0
  end subroutine tpoint_free

  !> Allocate memory in a listo tracking points
  subroutine trackpoints_memalloc(tp, ntime)
    implicit none
    class(trackpoints) :: tp
    integer :: ntime
    integer :: i
    do i = 1, tp%np
       call tp%p(i)%memalloc(ntime)
    end do
  end subroutine trackpoints_memalloc

  !> Clear the tracking points
  subroutine trackpoints_clear(tp)
    implicit none
    class(trackpoints) :: tp
    integer :: i
    if(allocated(tp%p)) then
       do i = 1, tp%np
          call tp%p(i)%free
       end do
       deallocate(tp%p)
    end if
    tp%np = 0
  end subroutine trackpoints_clear

  !> Load the tracking points from a file
  subroutine trackpoints_load(tp, fitrack)
    implicit none
    class(trackpoints) :: tp
    character(len=*) :: fitrack  ! Input file
    integer :: nuitp          ! File unit number of the list of tracking points
    real :: xtmp, ytmp, ztmp  ! Temporary values (real)
    character(len=10) :: ltmp ! Temporary string
    integer :: ntmp           ! Temporary value (integer)
    integer :: nrec           ! Number of read records
    logical :: is_fitrack     ! True if the tracking point file exists
    real, allocatable :: zlayer(:)  ! Coordinate of the center of the z-layer
    character(len=80) :: record
    integer :: i, status
    ! Clear
    call tp%clear
    ! Read the tracking points
    inquire(file=fitrack, exist=is_fitrack)
    if(is_fitrack) then
       call get_free_unit(nuitp)
       if(nuitp < 0) stop 'Error from tracking_points_load::get_free_unit'
       open(nuitp, file=fitrack, status='old', err=3004)  ! Should not jump to 3004
       allocate(zlayer(nz))
       zlayer = 0.5*(cellzb(1:nz)+cellzb(2:nz+1))
       nrec = 1
       do
          read(nuitp,'(a)', err=3005, end=3005) record
          nrec = nrec + 1
          if(record(1:1) == '#') cycle
          exit
       end do
       read(record,*,err=3005, end=3005) ntmp
       ! Allocate memory
       allocate(tp%p(ntmp))
       do i = 1, ntmp
          read(nuitp,*, err=3005, end=3005) ltmp, xtmp, ytmp, ztmp
          nrec = nrec + 1
          ! Check if the point is inside the domain
          if(xtmp < utmxor*1000. .or. xtmp > (utmxor + nx*dxk)*1000. .or. &
               ytmp < utmyor*1000. .or. ytmp > (utmyor + ny*dyk)*1000.) then
             write(*,'(''Tracking point: '',a,'' is out of domain (skipped)'')') trim(ltmp)
             cycle
          end if
          if(ztmp < cellzb(1) .or. ztmp > cellzb(nz+1)) cycle
          ! Find the weights and indices and store the point
          tp%np = tp%np + 1     ! Increment the number of tracking points
          tp%p(tp%np)%label = ltmp
          tp%p(tp%np)%x = xtmp
          tp%p(tp%np)%y = ytmp
          tp%p(tp%np)%z = ztmp
          call find_weights_uniform(1000.0*utmxor, nx, dx, tp%p(tp%np)%x, tp%p(tp%np)%xind, tp%p(tp%np)%xwgt, status)
          call find_weights_uniform(1000.0*utmyor, ny, dy, tp%p(tp%np)%y, tp%p(tp%np)%yind, tp%p(tp%np)%ywgt, status)
          call find_weights_nonuniform(zlayer, nz, tp%p(tp%np)%z, tp%p(tp%np)%zind, tp%p(tp%np)%zwgt, status)
       end do
       close(nuitp)
       deallocate(zlayer)
    end if
    return
3004 write(*,'(''Error: Cannot open file: '',a)') trim(fitrack)
    stop
3005 write(*,'(''Error reading file: '',a,'' near record'',i5 )') trim(fitrack),nrec
    stop
  end subroutine trackpoints_load

  !> Add the data to the tracking points
  subroutine trackpoints_addtime(tp,nx, ny, nz, time, u, v, w)
    implicit none
    class(trackpoints) :: tp
    real :: time    ! This is the time in format HHMM
    integer :: nx, ny, nz
    real :: u(nx,ny,nz)
    real :: v(nx,ny,nz)
    real :: w(nx,ny,nz)
    integer :: ip, it
    do ip = 1, tp%np   ! Loop on the tracking points
       it = tp%p(ip)%ntime + 1   ! Index of the time
       if(it > size(tp%p(ip)%time)) cycle  ! Check for memory overflow
       tp%p(ip)%ntime = it
       tp%p(ip)%time(it) = time
       call interpolate_3d(nx, ny, nz, u, tp%p(ip)%u(it), tp%p(ip)%xind, &
            tp%p(ip)%xwgt, tp%p(ip)%yind, tp%p(ip)%zwgt, tp%p(ip)%zind, tp%p(ip)%zwgt)
       call interpolate_3d(nx, ny, nz, v, tp%p(ip)%v(it), tp%p(ip)%xind, &
            tp%p(ip)%xwgt, tp%p(ip)%yind, tp%p(ip)%zwgt, tp%p(ip)%zind, tp%p(ip)%zwgt)
       call interpolate_3d(nx, ny, nz, w, tp%p(ip)%w(it), tp%p(ip)%xind, &
            tp%p(ip)%xwgt, tp%p(ip)%yind, tp%p(ip)%zwgt, tp%p(ip)%zind, tp%p(ip)%zwgt)
    end do
  end subroutine trackpoints_addtime

  subroutine trackpoints_print(tp, wdir)
    implicit none
    class(trackpoints) :: tp
    character(len=*) :: wdir  ! Output directory
    real :: wind_speed, wind_direction
    integer :: ip, it
    integer :: intime          ! Integer version of the time HHMM
    character(len=4) :: stime  ! String version of the time  HHMM
    character(len=255) :: fname
    integer :: nout
    do ip = 1, tp%np
       write(fname,'(a,''/tracking_point_'',a,''.dat'')') trim(wdir), trim(tp%p(ip)%label)
       call get_free_unit(nout)
       open(nout, file=fname, status='unknown', err=900)
       write(nout, '(''# Tracking point: '',a,2(1x,f10.2),1x,f8.2)') &
            trim(tp%p(ip)%label), tp%p(ip)%x, tp%p(ip)%y, tp%p(ip)%z
       do it = 1, tp%p(ip)%ntime
          ! Write cartesian
          ! write(*,*) tp%p(ip)%time(it), tinc, tp%p(ip)%u(it), tp%p(ip)%v(it), tp%p(ip)%w(it)
          ! Write polar (azimuth clockwise, 0=North)
          wind_speed = sqrt(tp%p(ip)%u(it)**2 + tp%p(ip)%v(it)**2)
          wind_direction = modulo(deg(atan2(tp%p(ip)%u(it), tp%p(ip)%v(it))), 360.0)
          ! Integer version of the time
          intime = nint(tp%p(ip)%time(it))
          write(stime, '(i4.4)') intime
          write(nout,*) stime, nint(tinc), wind_speed, wind_direction
       end do
       close(nout)
       cycle
900    write(*,'(''Error: cannot open file: '',a)') trim(fname)
    end do
  end subroutine trackpoints_print

  subroutine get_free_unit(unit)
    ! Returns the first free file unit number (returns -1 if not found)
    implicit none
    integer, parameter :: MAX_FILE_UNIT_NUMBER = 20000
    integer, intent(out) :: unit
    logical :: lod
    integer :: status
    integer :: i
    unit = -1
    do i = 2, MAX_FILE_UNIT_NUMBER                    ! Start from unit=2
       if(i == 5 .or. i == 6 .or. i == 7 .or. i == 9) cycle  ! Skip 5,6,7,9
       inquire(i, opened=lod, iostat = status)
       if(status /= 0) cycle
       if(.not.lod) then
          unit=i
          return
       end if
    end do
  end subroutine get_free_unit

end module m_trackpoints
