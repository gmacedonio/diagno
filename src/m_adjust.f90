module m_adjust

contains

  SUBROUTINE ADJUST(U,V,PHI,HTOPO,HBAR,UB,VB)
    !
    !     DIAGNO   VERSION 1.1  LEVEL 900221  ADJUST
    !
    use m_domain

    real :: U(:,:,:), V(:,:,:)
    real :: PHI(:,:,:), HTOPO(:,:)
    real :: UB(:,:,:), VB(:,:,:)
    !
    !      THIS ROUTINE ADJUSTS SURFACE WINDS FOR TERRAIN EFFECTS
    !
    !     INPUTS:  U (R ARRAY)     - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY)     - GRIDDED Y-DIRECTION WIND COMPONENTS
    !              HTOPO (R ARRAY) - GRIDDED TERRAIN HEIGHTS
    !              HBAR (R ARRAY)  - MODEL LEVELS
    !              UB (R ARRAY)    - U-COMPONENT BOUNDARY VALUES
    !              VB (R ARRAY)    - V-COMPONENT BOUNDARY VALUES
    !
    !     OUTPUTS:  U (R ARRAY) - X-DIRECTION WIND COMPONENTS WITH
    !                             ADJUSTED SURFACE LAYER WINDS
    !               V (R ARRAY) - Y-DIRECTION WIND COMPONENTS WIHT
    !                             ADJUSTED SURFACE LAYER WINDS
    !
    !     ITERATION CRITERIA
    !
    DATA ITMAX,EPSI,OVREL/75,0.02,1.5/
    KK = 1
    !
    !     COMPUTE TERRAIN GRADIENTS AND INITIAL POTENTIAL
    !
    DXI=0.5/DX
    DYI=0.5/DY
    FX=DXI/(HBAR)
    FY=DYI/(HBAR)
    DO J=1,NY
       DO I=1,NX
          HTOIM1=HTOPO(I,J)
          HTOJM1=HTOPO(I,J)
          IF(I.GT.1) HTOIM1=HTOPO(I-1,J)
          IF(J.GT.1) HTOJM1=HTOPO(I,J-1)
          HTOIP1=HTOPO(I,J)
          HTOJP1=HTOPO(I,J)
          IF(I.LT.NX) HTOIP1=HTOPO(I+1,J)
          IF(J.LT.NY) HTOJP1=HTOPO(I,J+1)
          DHDX=(HTOIP1-HTOIM1)*FX
          DHDY=(HTOJP1-HTOJM1)*FY
          PHI(I,J,2)=(U(I,J,KK)*DHDX+V(I,J,KK)*DHDY)
       end DO
    end DO
    !
    !     SET BOUNDARY VALUES FOR PHI
    !
    DO J=1,NY
       UB(J,1,1)=PHI(1,J,KK)
       UB(J,2,1)=PHI(NX,J,KK)
    end DO
    DO I=1,NX
       VB(I,1,1)=PHI(I,1,KK)
       VB(I,2,1)=PHI(I,NY,KK)
    end DO
    !
    !     SOLVE POISSON EQUATION BY GAUSS-SEIDEL METHOD FOR
    !     VELOCITY POTENTIAL
    !
    DXSQ=DX*DX
    DYSQ=DY*DY
    DSQ=DXSQ*DYSQ
    FACT=1.0/(2.0*(DXSQ+DYSQ))
    DO IT=1,ITMAX
       DO IDIR=1,4
          ERROR=-1.0E+09
          DO JJ=1,NY
             DO II=1,NX
                select case (idir)
                case (1)
                   I=II
                   J=JJ
                case (2)
                   I=NX-II+1
                   J=JJ
                case (3)
                   I=II
                   J=NY-JJ+1
                case (4)
                   I=NX-II+1
                   J=NY-JJ+1
                end select
                XOLD=PHI(I,J,KK)
                PHIIM1=UB(J,1,1)
                IF(I.GT.1)PHIIM1=PHI(I-1,J,KK)
                PHIJM1=VB(I,1,1)
                IF(J.GT.1)PHIJM1=PHI(I,J-1,KK)
                PHIIP1=UB(J,2,1)
                IF(I.LT.NX)PHIIP1=PHI(I+1,J,KK)
                PHIJP1=VB(I,2,1)
                IF(J.LT.NY)PHIJP1=PHI(I,J+1,KK)
                XX=DYSQ*(PHIIP1+PHIIM1)
                YY=DXSQ*(PHIJP1+PHIJM1)
                PHI(I,J,KK) = (1.-OVREL)*PHI(I,J,KK) &
                     + OVREL*FACT*(XX+YY-DSQ*PHI(I,J,2))
                IF(I.EQ.1) UB(J,1,1)=PHI(1,J,KK)
                IF(I.EQ.NX) UB(J,2,1)=PHI(NX,J,KK)
                IF(J.EQ.1) VB(I,1,1)=PHI(I,1,KK)
                IF(J.EQ.NY) VB(I,2,1)=PHI(I,NY,KK)
                IF(XOLD.EQ.0.) cycle
                IF(ABS(XOLD).LT.1.0E-10) cycle
                ERR=ABS((PHI(I,J,KK)-XOLD)/XOLD)
                ERROR=AMAX1(ERR,ERROR)
             end DO
          end DO
       end DO
       IF (ERROR.LE.EPSI) exit
    end DO
    !
    !     COMPUTE WIND COMPONENTS FROM VELOCITY POTENTIAL
    !
    DO J=1,NY
       DO I=1,NX
          PHIIM1=UB(J,1,1)
          PHIJM1=VB(I,1,1)
          PHIIP1=UB(J,2,1)
          PHIJP1=VB(I,2,1)
          IF(I.GT.1) PHIIM1=PHI(I-1,J,KK)
          IF(J.GT.1) PHIJM1=PHI(I,J-1,KK)
          IF(I.LT.NX) PHIIP1=PHI(I+1,J,KK)
          IF(J.LT.NY) PHIJP1=PHI(I,J+1,KK)
          U(I,J,KK)=(PHIIP1-PHIIM1)*DXI+U(I,J,KK)
          V(I,J,KK)=(PHIJP1-PHIJM1)*DYI+V(I,J,KK)
       end DO
    end DO
    !
    !     RESET BOUNDARY ARRAY
    !
    DO K=1,2
       DO J=1,NY
          UB(J,K,1)=0.
       end DO
       DO I=1,NX
          VB(I,K,1)=0.
       end DO
    end DO
    RETURN
  end SUBROUTINE ADJUST

end module m_adjust
