module m_windbc

contains

  SUBROUTINE WINDBC(U,V,UB,VB,K)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  WINDBC
    !
    use m_domain
    implicit none

    integer :: k
    real :: U(:,:,:), V(:,:,:), UB(:,:,:), VB(:,:,:)
    integer :: i, j
    !
    !     SETS BOUNDARY CONDITIONS FOR WIND FIELDS
    !     NO INFLOW - NO OUTFLOW BOUNDARY CONDITIONS ARE USED
    !
    !     INPUTS:  U (R ARRAY) - GRIDDED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY) - GRIDDED Y-DIRECTION WIND COMPONENTS
    !              K (I)       - VERTICAL LEVEL INDEX
    !
    !     OUTPUTS:  UB (R ARRAY) - U-COMPONENT BOUNDARY VALUES
    !               VB (R ARRAY) - V-COMPONENT BOUNDARY VALUES
    !
    !
    !     SET BOUNDARY VELOCITIES
    !
    DO J=1,NY
       UB(J,1,K)=U(1,J,K)
       UB(J,2,K)=U(NX,J,K)
    end do
    DO I=1,NX
       VB(I,1,K)=V(I,1,K)
       VB(I,2,K)=V(I,NY,K)
    end do
    RETURN
  END SUBROUTINE WINDBC

end module m_windbc
