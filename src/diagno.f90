PROGRAM DIAGNO
  !
  !     This version was modified starting from version 1.1
  !     Date: 9-NOV-2023
  !
  !*****************************************************************
  !
  !
  !     DIAGNO VERSION 1.1   LEVEL 900221   R. KESSLER, S. DOUGLAS, SAI
  !
  !     DIAGNO IS A DIAGNOSTIC WIND MODULE WHICH GENERATES A THREE-
  !     DIMENSIONAL, NON-DIVERGENT WIND FIELD IN TERRAIN FOLLOWING
  !     COORDINATES.  BEGINNING WITH A DOMAIN-SCALE MEAN WIND, THE
  !     VERTICAL VELOCITY DUE TO TOPOGRAPHIC EFFECTS IS CALCULATED.
  !     THE HORIZONTAL VELOCITY FIELD IS THEN ADJUSTED TO MINIMIZE
  !     THE DIVERGENCE SUBJECT TO THE CONSTRAINT THAT THE VERTICAL
  !     VELOCITY IS HELD CONSTANT.  THERMALLY-INDUCED SLOPE FLOWS
  !     ARE GENERATED AND ADDED TO THE WIND FIELD.  A FROUDE NUMBER
  !     ADJUSTMENT PROCEDURE IS USED TO DIVERT THE FLOW AROUND
  !     TERRAIN OBSTACLES.  THE FIRST-GUESS FIELD IS COMBINED WITH
  !     OBSERVATIONS TO PRODUCE A GRIDDED WIND FIELD.  FINALLY A NEW
  !     VERTICAL VELOCITY IS CALCULATED USING THE CONTINUITY EQUATION
  !     AND IS ADJUSTED SO THAT THE VERTICAL VELOCITY AT THE TOP OF
  !     THE MODEL IS ZERO.  THE DIVERGENCE IS MINIMIZED SUBJECT TO
  !     THE CONSTRAINT THAT THE VERTICAL VELOCITY IS UNCHANGED.
  !
  !     THE DIAGNOSTIC WIND MODULE IS AN ENHANCED VERSION OF THE CIT
  !     WIND MODEL (GOODIN,MCRAE AND SEINFELD, 1980).  THE FROUDE
  !     NUMBER MODIFICATION SCHEME IS ADAPTED FROM THE MELSAR MODEL
  !     (ALLWINE AND WHITEMAN, 1985).  THE KINEMATIC AND SLOPE FLOW
  !     PARAMETERIZATIONS ARE DERIVED FORM THE COMPLEX-TERRAIN WIND
  !     MODEL (YOCKE, 1979).
  !
  !     DIAGNO IS CURRENTLY DIMENSIONED AS FOLLOWS:
  !
  !     50 X 50 GRID POINTS IN THE X & Y DIRECTIONS
  !          15 VERTICAL LAYERS
  !         100 WIND STATIONS (SURFACE + UPPER AIR)
  !          50 UPPER AIR WIND STATIONS
  !          20 BARRIERS
  !
  use m_adjust
  use m_barier
  use m_divcel
  use m_divpr
  use m_domain
  use m_fradj
  use m_inter1
  use m_inter2
  use m_minim
  use m_outfil
  use m_rtheta
  use m_slope
  use m_smooth
  use m_terset
  use m_topof2
  use m_trackpoints
  use m_windbc
  use m_wndlpt
  use m_windpr
  use m_wndpr2
  implicit real (a-h, o-z)

  !
  CHARACTER(len=4), allocatable :: NAMST(:)
  CHARACTER(len=4) :: LAST,IBLANK,NAM
  CHARACTER(20) TITLE
  character(512) wdir
  !
  real, allocatable :: u(:,:,:), v(:,:,:), w(:,:,:)
  real, allocatable :: uslope(:,:,:), vslope(:,:,:)
  real, allocatable :: ug(:,:,:), vg(:,:,:), div(:,:,:)
  real, allocatable :: ub(:,:,:), vb(:,:,:)
  real, allocatable :: htopo(:,:), hmax(:,:), us(:,:), vs(:,:), alw(:,:)
  integer, allocatable :: lndwtr(:,:)
  type(trackpoints)  :: tp       ! Tracking points
  character(len=255) :: fitrack  ! Name of the input tracking point file
  !
  real, allocatable :: utmxst(:), utmyst(:), wt(:), rs(:)
  real, allocatable :: cellzc(:)
  real, allocatable :: work(:)
  integer, allocatable :: IS(:), IST(:), JST(:)
  real :: work2(3)

  DATA ZERO /0./, IZERO /0/, LAST /'LAST'/, IBLANK /'    '/, IONE/1/
  !
  !     SET EDIT FLAGS
  !
  EDIT = 999.
  EDITL= 900.
  IEDIT = 999
  IEDITL = 900
  !
  !     SET LOGICAL UNITS
  !
  IRD = 12
  IRD1 = 7
  IRD2 = 8
  !      IRDT = 5
  !      IRDL = 4
  IWR = 3
  IFILE = 9
  IFILEK = 11
  IFILEF = 13
  IFILES = 14
  !
  if(command_argument_count() >= 1) then
     call get_command_argument(1, wdir)
  else
     wdir = '.'
  end if
  !
  !     OPEN INPUT AND OUTPUT FILES
  !
  OPEN(UNIT=IRD,FILE=TRIM(wdir)//'/'//'diagno.inp',STATUS='OLD', err=3001)
  !      OPEN(UNIT=IRDT,FILE='topog.mat',STATUS='OLD')
  !      OPEN(UNIT=IRDL,FILE='topog.lw',STATUS='OLD')
  OPEN(UNIT=IRD1,FILE=TRIM(wdir)//'/'//'surface.dat',STATUS='OLD', err=3002)
  OPEN(UNIT=IRD2,FILE=TRIM(wdir)//'/'//'upper.dat',STATUS='OLD', err=3003)
  OPEN(UNIT=IWR,FILE=TRIM(wdir)//'/'//'diagno.prt',STATUS='unknown')
  OPEN(UNIT=IFILE,FILE=TRIM(wdir)//'/'//'diagno.out',STATUS='UNKNOWN', &
       FORM='UNFORMATTED')
  !RS   OPEN(UNIT=IFILEK,FILE='DIAGNO.KIN',STATUS='NEW',FORM='UNFORMATTED')
  !RS   OPEN(UNIT=IFILEF,FILE='DIAGNO.FRD',STATUS='NEW',FORM='UNFORMATTED')
  !RS   OPEN(UNIT=IFILES,FILE='DIAGNO.SLP',STATUS='NEW',FORM='UNFORMATTED')
  !
  !     READ INITIAL INPUT DATA
  !
  READ(IRD,1035)  TITLE
  !
  CALL SETUP
  !
  ! Allocate memory and clear
  allocate(u(nx,ny,nz))
  allocate(v(nx,ny,nz))
  allocate(w(nx,ny,nz+1))
  allocate(uslope(nx,ny,nz))
  allocate(vslope(nx,ny,nz))
  allocate(ug(nx,ny,nz))
  allocate(vg(nx,ny,nz))
  allocate(div(nx,ny,nz))
  allocate(work(nx*ny))
  allocate(ub(ny,2,nz))
  allocate(vb(nx,2,nz))
  allocate(htopo(nx,ny))
  allocate(hmax(nx,ny))
  allocate(us(nz,nwind))
  allocate(vs(nz,nwind))
  allocate(lndwtr(nx,ny))
  allocate(alw(nx,ny))
  allocate(namst(nwind))
  allocate(utmxst(nwind))
  allocate(utmyst(nwind))
  allocate(wt(nwind))
  allocate(rs(nwind))
  allocate(is(nwind))
  allocate(ist(nwind))
  allocate(jst(nwind))
  allocate(cellzc(nz))
  !
  ! Clear
  u = 0.0
  v = 0.0
  w = 0.0
  ug = 0.0
  vg = 0.0
  ub = 0.0
  vb = 0.0
  !
  !     INITIALIZE SOME PARAMETERS
  !
  IFIN = 0
  NZP1=NZ+1
  NSURF=NWIND-NUPPER
  NOHRS = int((TEND - TSTART)/100) + 1
  if(NOHRS .gt. 24) then
     write(*,'(''Error: NOHRS greater than 24 hours'')')
     call exit(1)
  end if
  !
  !     CONVERT HORIZONTAL DIMENSIONS TO METERS
  !
  DX = DXK*1000.
  DY = DYK*1000.
  !
  !     CALCULATE CELL DEPTHS AND CELL CENTER HEIGHTS
  !     (DZ IS VERTICAL CELL DEPTH; CELLZC IS VERTICAL CELL CENTER
  !      ELEVATION)
  !
  DO I=1,NZ
     DZ(I) = CELLZB(I+1)  -  CELLZB(I)
     CELLZC(I) = CELLZB(I) + .5*DZ(I)
  end do
  !
  !
  ! Load the tracking points
  fitrack=trim(wdir)//'/'//'tracking_points.dat' ! Tracking point file
  call tp%load(fitrack)     ! Load the coordinates of the tracking points
  call tp%memalloc(nohrs)   ! Allocate memory for storing time data
  !
  !     ECHO INITIAL INPUT DATA                                      .
  !
  WRITE(IWR,2000) TITLE
  WRITE(IWR,2030)
  WRITE(IWR,2040)
  WRITE(IWR,2050) NX,NY,NZ
  WRITE(IWR,2060) DX,DY,(DZ(I),I=1,NZ)
  WRITE(IWR,2061) (CELLZC(I),I=1,NZ)
  WRITE(IWR,2070)
  WRITE(IWR,2080)
  WRITE(IWR,2090) NOHRS,NWIND
  WRITE(IWR,2091) NSURF,NUPPER
  WRITE(IWR,2092) IPR0,IPR1,IPR2,IPR3,IPR4
  WRITE(IWR,2093) IPR5,IPR6,IPR7,IOUTD,ICALC
  IF(NUMBAR .NE. 0) then
     IF(NWIND .GT. 30)  WRITE(IWR,2000) TITLE
     WRITE(IWR,2150)
     WRITE(IWR,2151)
     WRITE(IWR,2152)
     DO N=1,NUMBAR
        I1 = int(BARXY(1,N)/DXK) + 1
        J1 = int(BARXY(2,N)/DYK) + 1
        I2 = int(BARXY(3,N)/DXK) + 1
        J2 = int(BARXY(4,N)/DYK) + 1
        WRITE(IWR,2153) N,I1,J1,I2,J2
     end do
  end if
  WRITE(IWR,2261) NITER
  WRITE(IWR,2262) DIVLIM
  WRITE (IWR,2265) IOBR, I3DCTW
  WRITE(IWR,2263) RMIN, RMAX1, RMAX2, RMAX3
  WRITE(IWR,2264) (NINTRP(I), I = 1,NZ)
  IF (I3DCTW .EQ. 1) THEN
     WRITE(IWR,3000)
     WRITE(IWR,3010)
     WRITE(IWR,3012) R1,R2
     WRITE(IWR,3015) (TGAMMA(I),I=1,24)
     WRITE(IWR,3020) TINF
     WRITE(IWR,3030) CRITFN,TERRAD
     WRITE(IWR,3040) (BETA2(I), I = 1,24)
     WRITE(IWR,3050) IFRADJ,IKINE,ALPHA
     WRITE(IWR,3060) (UM(I), I = 1,24)
     WRITE(IWR,3070) (VM(I), I = 1,24)
  ENDIF
  !
  !     INITIALIZE TERRAIN ARRAYS
  !
  htopo = zero
  hmax  = zero
  lndwtr = izero
  !
  !     READ TERRAIN DATA
  !     HTOPO IS HEIGHT OF TOPOGRAPHY (UNITS OF HTFAC*METERS), FOR
  !     EACH GRID SQUARE, ABOVE A BASE ELEVATION (E.G.,MSL)
  !
  IF(HTFAC .GT. 0.) then
     !     DO 118 J = NY,1,-1
     !     DO 118 J = 1,NY
     !     READ (IRDT,1808) (HTOPO(I,J),I=1,NX)
     !     READ (IRDT,*) (HTOPO(I,J),I=1,NX)
     !     118 CONTINUE
     !
     !     @@@ Changed
     !     CALL READGRD (New method: read and interpolate GRD file)
     xorig = 1000.*utmxor   ! Convert to meters
     yorig = 1000.*utmyor   ! Convert to meters
     !
     call readgrd(TRIM(wdir)//'/'//'topography.grd',xorig,yorig,dx,dy,nx,ny, &
          nx,ny,htopo)
  end if
  !
  !     CONVERT TERRAIN HEIGHT TO METERS
  !
  ! @@@ INITIALIZE HTMIN and HTMAX
  HTMIN =  1.0e10
  HTMAX = -1.0e10
  !
  do j=1,ny
     do i=1,nx
        htopo(i,j)=htopo(i,j)*htfac
        htmin=amin1(htopo(i,j),htmin)
        htmax=amax1(htopo(i,j),htmax)
     end do
  end do
  !
  !     SET UP THE HMAX ARRAY
  !
  IF (I3DCTW .EQ. 1) CALL TERSET(HTOPO,HMAX)
  !
  !     OUTPUT TERRAIN DATA
  !
  WRITE(IWR,2000) TITLE
  WRITE(IWR,2141)
  WRITE(IWR,2142)
  CALL WNDLPT(HTOPO)
  RANGE=HTMAX-HTMIN
  !
  WRITE(IWR,2143)
  WRITE(IWR,2144)
  CALL WNDLPT(HMAX)
  !
  !     READ IN LNDWTR TYPE ARRAY
  !
  !     DO 119 J = NY,1,-1
  !     DO 119 J = 1,NY
  !     READ(IRDL,1809) (LNDWTR(I,J),I=1,NX)
  !     119 CONTINUE
  !
  ! @@@@ Set land-use according to topography
  do j=1,ny
     do i=1,nx
        if(htopo(i,j) .eq. 0.0) then
           lndwtr(i,j) = 0   ! Z=0 ==> SEA
        else
           lndwtr(i,j) = 1   ! Z>0 ==> LAND
        end if
     end do
  end do
  !
  !     OUTPUT LNDWTR TYPE DATA
  !
  WRITE(IWR,2000) TITLE
  WRITE(IWR,2145)
  WRITE(IWR,2146)
  do j = 1,ny
     do i = 1,nx
        alw(i,j) = lndwtr(i,j)
     end do
  end do
  CALL WNDLPT(ALW)
  !
  !     FOR THE CASE OF NO OBSERVATIONS, PROCEED DIRECTLY TO THE
  !     CALCULATION OF THE DIAGNOSTIC WIND FIELD
  !
  IF (NWIND .GT. 0) then
     !
     !     READ WIND STATION NAMES AND UTM COORDINATES (IN KM)
     !
     READ(IRD1,1030) (NAMST(I),UTMXST(I),UTMYST(I),I=1,NSURF)
     I1 = NSURF + 1
     READ(IRD2,1030) (NAMST(I),UTMXST(I),UTMYST(I),I=I1,NWIND)
     !
     !     CONVERT TO LOCAL COORDINATES
     !
     DO I=1,NWIND
        !     write(*,*) 'P',utmxst(i),utmyst(i)
        UTMXST(I) = UTMXST(I) - UTMXOR
        UTMYST(I) = UTMYST(I) - UTMYOR
        IST(I) = int(UTMXST(I)/DXK) + 1
        JST(I) = int(UTMYST(I)/DYK) + 1
        !     write(*,*) 'D',utmxst(i),utmyst(i)
        !     write(*,*) utmxor,utmyor
        !     write(*,*) dxk,dyk
        !     write(*,*) ist(i),jst(i)
        !     if(LNDWTR(IST(I),JST(I)) .eq. 0) then
        !     write(*,*) 'Station in the SEA '
        !     write(*,*) NAMST(I),IST(I),JST(I),LNDWTR(IST(I),JST(I))
        !     endif
        !
     end do
     WRITE(IWR,2110)
     WRITE(IWR,2120)
     WRITE(IWR,2130)
     DO I=1,NWIND
        WRITE(IWR,2140) I,IST(I),JST(I),NAMST(I)
        IF(I.NE.30) cycle
        WRITE(IWR,2000) TITLE
        WRITE(IWR,2110)
        WRITE(IWR,2120)
        WRITE(IWR,2130)
     end do
  end if
  !
  !     BEGIN SIMULATION                                          .
  !
  TIME = TSTART
  DO NH=1,NOHRS
     IT = IFIX(TIME/100.) + 1
     IF (I3DCTW .EQ. 1) then
        !
        !     SET UP FIRST GUESS FIELD
        !
        do k = 1,nz
           do j = 1,ny
              do i = 1,nx
                 ug(i,j,k) = um(it)
                 vg(i,j,k) = vm(it)
              end do
           end do
        end do
        !
        !     SET BOUNDARY CONDITIONS
        !
        do k=1,nz
           call windbc(ug,vg,ub,vb,k)
        end do
        !
        !     INITIALIZE THE VERTICAL VELOCITY
        !
        w = zero
        !
        !     CALCULATE THE VERTICAL VELOCITY DUE TO TOPOGRAPHIC EFFECTS
        !
        IF (IKINE .EQ. 1) CALL TOPOF2(UG,VG,W,HTOPO,TIME)
        !
        !     GENERATE A NEW HORIZONTAL VELOCITY FIELD USING THE
        !     DIVERGENCE MINIMIZATION PROCEDURE
        !
        IF (IKINE .EQ. 1) CALL MINIM(UG,VG,W,UB,VB,DIV)
        !
        !     OUTPUT KINEMATIC EFFECTS
        !
        IF (IPR5 .GT. 0) then
           WRITE(IWR,2000) TITLE
           WRITE(IWR,2904)
           WRITE(IWR,2905)
           CALL WINDPR(UG,VG,W)
           IF (IOUTD .GT. 0) CALL OUTFIL(IFILEK,TIME,UG,VG,W)
        end if
        !
        !     CALCUATE THE SLOPE FLOW
        !
        CALL SLOPE(USLOPE,VSLOPE,HTOPO,TIME,HMAX)
        !
        !     ADD THE SLOPE FLOW COMPONENT TO THE HORIZONTAL WIND FIELD
        !
        do k=1,nz
           do j=1,ny
              do i=1,nx
                 ug(i,j,k)=ug(i,j,k)+uslope(i,j,k)
                 vg(i,j,k)=vg(i,j,k)+vslope(i,j,k)
              end do
           end do
        end do
        !
        !     OUTPUT SLOPE FLOW EFFECTS
        !
        IF (IPR6 .GT. 0) then
           WRITE(IWR,2000) TITLE
           WRITE(IWR,2908)
           WRITE(IWR,2909)
           CALL WNDPR2(UG,VG)
           IF (IOUTD .GT. 0) CALL OUTFIL(IFILES,TIME,UG,VG,W)
        end if
        !
        !     APPLY THE MELSAR FROUDE NUMBER ADJUSTMENT
        !
        IF (IFRADJ .EQ. 1) CALL FRADJ(UG,VG,HTOPO,HMAX,TIME)
        !
        !     OUTPUT FROUDE NUMBER EFFECTS
        !
        IF (IPR7 .GT. 0) then
           WRITE(IWR,2000) TITLE
           WRITE(IWR,2906)
           WRITE(IWR,2907)
           CALL WNDPR2(UG,VG)
           IF (IOUTD .GT. 0) CALL OUTFIL(IFILEF,TIME,UG,VG,W)
        end if
        !
        !     SET BOUNDARY CONDITIONS
        !
        do k=1,nz
           call windbc(ug,vg,ub,vb,k)
        end do
        !
        !     IN THE CASE OF NO OBSERVATIONS, PROCEED DIRECTLY TO THE
        !     SMOOTHING STEP
        !
        if (nwind .eq. 0) then
           do k = 1,nz
              do j = 1,ny
                 do i = 1,nx
                    u(i,j,k) = ug(i,j,k)
                    v(i,j,k) = vg(i,j,k)
                 end do
              end do
           end do
        endif
     end if
     IF (I3DCTW .EQ. 1 .AND. NWIND .EQ. 0) GO TO 285
     !
     !     READ HOURLY INPUT DATA
     !
     WRITE(IWR,2000) TITLE
     WRITE(IWR,2240) TIME, NH
     WRITE(IWR,2241)
     !
     !     READ SURFACE WIND DATA
     !
     IF(NSURF .NE. 0) then
        us = edit
        vs = edit
        do
           READ(IRD1,1043) NAM, (WORK2(J),J=1,3)
           IF(NAM .EQ. LAST) exit
           IF(NAM .EQ. IBLANK) cycle
           do j=1,nsurf
              if(nam.eq.namst(j)) go to 43
           end do
           WRITE(IWR,2069) TIME,NAM
           call exit(0)
43         WT(J) = WORK2(1)
           US(1,J)=WORK2(2)
           VS(1,J)=WORK2(3)
        end do
        !
        !     PRINT INPUT SURFACE WIND DATA
        !
        IF(IPR0.GT.0)  WRITE(IWR,2000) TITLE
        WRITE(IWR,2279)
        WRITE(IWR,2280)
        do i=1,nsurf
           write(iwr,2290) namst(i),us(1,i),vs(1,i),wt(i)
        end do
     end if
     !
     !     INPUT UPPER AIR WIND DATA
     !     (WINDS ARE INPUT FOR THE CELL CENTER ELEVATION)
     !
     IF(NUPPER .NE. 0) then
        N = NSURF + 1
        wt(n:n+nupper) = zero
        N = 2*NZ + 1
        do
           READ(IRD2,1041) NAM, (WORK(I),I=1,N)
           IF(NAM.EQ.LAST) exit
           N1 = NSURF + 1
           DO J=N1,NWIND
              IF(NAM.EQ.NAMST(J)) GO TO 54
           end do
           WRITE(IWR,2390) TIME,NAM
           call exit(0)
54         WT(J) = WORK(1)
           I1=1
           !
           !     IF A SURFACE STATION ALSO EXISTS AT THIS POINT,
           !     DO NOT USE UPPER AIR DATA IN LOWEST LAYER
           !
           do i = 1,nsurf
              if (nam .eq. namst(i)) i1 = 2
           end do
           IF (IEXTRP .LE. 0) I1=2
           do i=i1,nz
              k = 2*(i-1) + 2
              us(i,j) = work(k)
              vs(i,j) = work(k+1)
           end do
        end do
        !
        !     PRINT INPUT UPPER AIR WIND DATA
        !
        IF(NWIND.GT.30) WRITE(IWR,2000) TITLE
        NSET = NZ/5
        IF (MOD(NZ,5) .NE. 0) NSET = NSET + 1
        DO NS = 1,NSET
           N1 = (NS - 1) * 5 + 1
           N2 = NS * 5
           N2 = MIN0(N2,NZ)
           WRITE(IWR,2292) (I,I=N1,N2)
           WRITE(IWR,2293)
           DO L=1,NUPPER
              LST = NSURF + L
              WRITE(IWR,2294) NAMST(LST),(US(I,LST),VS(I,LST),I=N1,N2)
           end do
        end do
     end if
     IF(ICALC.LE.0) GO TO 850
     !
     !     EXTRAPOLATE SURFACE WINDS
     !     EXTRAPOLATION OPTIONS:
     !       1) IF IABS(IEXTRP)=1, THEN DO NOT EXTRAPOLATE FROM SURFACE DATA
     !       2) IF IABS(IEXTRP)=2, THEN USE POWER LAW
     !       3) IF IABS(IEXTRP)=3, THEN USE FEXTRP MULTIPLIER
     !       4) IF IEXTRP<=0, THEN DO NOT USE LEVEL 1 DATA FROM UA WINDS
     !
     IF(IABS(IEXTRP) .NE. 1) then
        RMIN2 = 4.
        DO L=1,NSURF
           IF(NUPPER .NE. 0) then
              !
              !     CHECK FOR CLOSE UPPER AIR STATION WITH VALID WIND DATA
              !     SKIP EXTRAPOLATION IF CLOSE UPPER AIR DATA EXISTS
              !
              N = NSURF + 1
              ! CALL XMIT(-NUPPER,EDIT,RS(N))
              rs(n:n+nupper) = edit
              !
              !     CHECK IF UPPER AIR STATION HAS SOME VALID DATA
              !
              DO I = 1,NUPPER
                 DO K = 1,NZ
                    IF(US(K,L).LT.EDITL .OR. VS(K,L) .LT. EDITL)  GO TO 62
                 end do
                 cycle
                 !
                 !     COMPUTE DISTANCES BETWEEN SURFACE STATION AND UPPER STATIONS
                 !
62               J = NSURF + I
                 RSX = (UTMXST(J)  - UTMXST(L))/DXK
                 RSY = (UTMYST(J)  - UTMYST(L))/DYK
                 RS(I) = RSX**2 + RSY**2
                 RS(I) = SQRT(RS(I))
              end do
              CALL FMINF(RS,NUPPER,FMIN,NMIN)
              IF(FMIN.LT.RMIN2) cycle
           end if
           IF(IABS(IEXTRP) .LT. 3) then
              !
              !     EXTRAPOLATE WITH POWER LAW PROFILE
              !
              IG = IST(L)
              JG = JST(L)
              DO K=2,NZ
                 KM1=K-1
                 IF(US(KM1,L).GT.EDITL .OR. VS(KM1,L).GT.EDITL) cycle
                 PEXP = 0.143
                 IF (HTOPO(IG,JG) .EQ. 0.) PEXP = 0.286
                 IF (K.GT.2) then
                    PADJ = ( CELLZC(K)/CELLZC(KM1) )**PEXP
                 else
                    PADJ = ( CELLZC(K)/ZSWIND )**PEXP
                 end if
                 US(K,L) = US(KM1,L)*PADJ
                 VS(K,L) = VS(KM1,L)*PADJ
              end do
              cycle
           end if
           !
           !     EXTRAPOLATE WITH USER'S EXTRAPOLATION MULTIPLIERS
           !
           DO K=2,NZ
              US(K,L) = US(1,L)*FEXTRP(K)
              VS(K,L) = VS(1,L)*FEXTRP(K)
           end do
        end do
     end if
     !
     !     INITIALIZE GLOBAL ARRAYS                                     .
     !
     u = zero
     v = zero
     div = zero
     w = zero
     !
     !     INTERPOLATE WIND FIELD IN EACH VERTICAL LAYER               .
     !
     IF (I3DCTW .EQ. 1) THEN
        CALL INTER2(US,NWIND,UTMXST,UTMYST,RS,IS,WORK,U,NUMBAR,UG,LNDWTR)
        CALL INTER2(VS,NWIND,UTMXST,UTMYST,RS,IS,WORK,V,NUMBAR,VG,LNDWTR)
     ELSE
        CALL INTER1(US,NWIND,UTMXST,UTMYST,RS,IS,WORK,U,NUMBAR)
        CALL INTER1(VS,NWIND,UTMXST,UTMYST,RS,IS,WORK,V,NUMBAR)
     ENDIF
     !
     !     OUTPUT INTERPOLATED WIND FIELD                            .
     !
     IF(IPR0 .GT. 0) then
        WRITE(IWR,2000) TITLE
        WRITE(IWR,2900)
        WRITE(IWR,2901)
        CALL WNDPR2(U,V)
     end if
     !
     !     ADJUST SURFACE LAYER WIND FOR TERRAIN EFFECTS
     !
     IF(HTFAC .GT. 0.) then
        IF (I3DCTW .NE. 1) CALL ADJUST(U,V,W,HTOPO,CELLZB(NZP1),UB,VB)
        !
        !     SET BOUNDARY CONDITIONS
        !
        DO K = 1,NZ
           CALL WINDBC(U,V,UB,VB,K)
        end do
        !
        !     OUTPUT ADJUSTED WIND FIELD                            .
        !
        IF(IPR1 .GT. 0) then
           WRITE(IWR,2000) TITLE
           WRITE(IWR,2902)
           WRITE(IWR,2903)
           CALL WNDPR2(U,V)
        end if
     end if
285  CONTINUE
     !
     !     SMOOTH HORIZONTAL FIELDS
     !
     CALL SMOOTH(U,V,UB,VB)
     !
     !     REINITIALIZE VERTICAL VELOCITY
     !
     w = zero
     !
     !     COMPUTE DIVERGENCE FIELDS AND VERTICAL VELOCITIES
     !
     DIVMAX=-1.0E+09
     DO K=1,NZ
        CALL DIVCEL(U,V,W,DIV,UB,VB,DIVMAX,K)
     end do
     !
     do k=1,nz
        do j=1,ny
           do i=1,nx
              w(i,j,k+1)=-dz(k)*div(i,j,k)+w(i,j,k)
           end do
        end do
        !
        !     SET BOUNDARY CONDITIONS
        !
        call windbc(u,v,ub,vb,k)
     end do
     !
     !     ADJUST VERTICAL VELOCITY AS SUGGESTED BY OBRIEN (1970)
     !     (JAM, V.9, NO.2), WITH ERROR IN W PROPORTIONAL TO HEIGHT
     !
     if (iobr .eq. 1) then
        do k=1,nz
           do j=1,ny
              do i=1,nx
                 w(i,j,k+1)=w(i,j,k+1)-cellzb(k+1)/cellzb(nzp1)*w(i,j,nzp1)
              end do
           end do
        end do
     endif
     !
     !     OUTPUT INITIAL WIND AND DIVERGENCE FIELDS
     !
     IF (IPR2 .GT. 0) then
        WRITE(IWR,2000) TITLE
        WRITE(IWR,2400)
        WRITE(IWR,2402)
        CALL WINDPR(U,V,W)
        WRITE(IWR,2401) DIVMAX
        CALL DIVPR(DIV)
     end if
     !
     !     ITERATE TO MINIMIZE DIVERGENCE
     !
     IF (IOBR .EQ. 1) CALL MINIM(U,V,W,UB,VB,DIV)
     !
     !     OUTPUT FINAL WIND FIELDS
     !
     WRITE(IWR,2000) TITLE
     WRITE(IWR,2800) TIME,NH
     WRITE(IWR,2801)
     CALL WINDPR(U,V,W)
     !
     !     OUTPUT FINAL DIVERGENCE FIELDS
     !
     IF(IPR4 .LE. 0) then
        WRITE(IWR,2000) TITLE
        WRITE(IWR,2803)
        CALL DIVPR(DIV)
     end if
     !
     !     OUTPUT FINAL WIND SPEED AND DIRECTION FIELDS
     !
     IF(IPR3 .GT. 0) then
        WRITE(IWR,2000) TITLE
        WRITE(IWR,2820) TIME,NH
        WRITE(IWR,2821)
        CALL RTHETA(U,V,DIV)
     end if
     !
     !     OUTPUT FINAL WIND FIELD
     !
     CALL OUTFIL(IFILE,TIME,U,V,W)
     !
     ! Update the tracking points
     call tp%addtime(nx,ny,nz,time,ug,vg,w)
     !
     !     INCREMENT TIME
     !
850  CONTINUE
     TIME = TIME + TINC * 100
  end DO
  !
  ! Print the tracking points
  call tp%print(wdir)
  !
  ! Free memory
  call tp%clear
  !
  !     FORMAT SPECIFICATIONS
  !
  !     1010 FORMAT(30X,10I5)
  !     1021 FORMAT(30X,10F5.0)
  !     1020 FORMAT(30X,5F10.0)
1030 FORMAT(20X,A4,2F8.1)
  !     1033 FORMAT(20X,3I3,1X,10F5.0)
1035 FORMAT(A20)
1041 FORMAT(13X,A4,1X,31F5.1)
1043 FORMAT(15X,A4,1X,3F5.1)
  !     1808 FORMAT(10F6.0)
  !     1809 FORMAT(30I2)
  !     1809 FORMAT(5000I2)
2000 FORMAT(1X,A20,/)
2030 FORMAT(//,5X,'GRID DESCRIPTION')
2040 FORMAT(5X,16('-'))
2050 FORMAT(10X,'NX = ',I8,2X,'NY = ',I8,2X,'NZ = ',I8)
2060 FORMAT(10X,'DX = ',F8.1,2X,'DY = ',F8.1,2X,'DZ = ',10F8.1,/,40X,'DZ = ',10F8.1)
2061 FORMAT(10X,'CELL CENTER HEIGHTS = ',9X,5F8.1)
  !     2065 FORMAT(2X,'JOB ABORTED -- UNRECOGNIZED STABILITY STATION NAME
  !     1       INPUT AT TIME = ',F6.0,1X,'NAME = ',A4)
  !     2066 FORMAT(2X,'JOB ABORTED -- UNRECOGNIZED TEMP STATION NAME INPUT
  !     1       AT TIME = ',F6.0,1X,'NAME = ',A4)
2069 FORMAT(2X,'JOB ABORTED -- UNRECOGNIZED STATION NAME FOR SURFACE &
       &WIND DATA AT TIME = ',F6.0,1X,'NAME = ',A4)
2070 FORMAT(//,5X,'SIMULATION OPTIONS')
2080 FORMAT(5X,18('-'))
2092 FORMAT(10X,'IPR0=',I5,2X,'IPR1=',I5,2X,'IPR2=',I5,2X,'IPR3=',I5,2X, &
       'IPR4=',I5)
2093 FORMAT(10X,'IPR5=',I5,2X,'IPR6=',I5,2X,'IPR7=',I5,2X,'IOUTD=',I4,2X,'ICALC=',I4)
2091 FORMAT(10X,'NSURF = ',I4,5X,'NUPPER = ',I3)
2090 FORMAT(10X,'NOHRS = ',I4,5X,'NWIND = ',I4)
2110 FORMAT(//,5X,'GRIDDED WIND STATIONS COORDINATES')
  !     2111 FORMAT(//,5X,'GRIDDED STABILITY PROFILES LOCATIONS')
  !     2112 FORMAT(5X,36('-'))
2120 FORMAT(5X,33('-'))
2130 FORMAT(/,3X,'NO.',7X,'IS',8X,'JS',6X,'NAME'/)
2140 FORMAT(3X,I3,2I10,6X,A4)
2141 FORMAT(5X,'TERRAIN HEIGHTS (M)')
2142 FORMAT(5X,21('-'))
2143 FORMAT(/,5X,'HMAX ARRAY')
2144 FORMAT(5X,10('-'))
2145 FORMAT(/,5X,'SURFACE TYPE')
2146 FORMAT(5X,12('-'))
2150 FORMAT(//,5X,'GRIDDED BARRIER END-POINT LOCATIONS')
2151 FORMAT(5X,35('-'))
2152 FORMAT(/,3X,'NO.',7X,'I1,J1',5X,'I2,J2',/)
2153 FORMAT(3X,I2,7X,2I3,4X,2I3)
  !     2210 FORMAT(//,12X,'STABILITY PROFILE DATA',/,12X,22('-'),//,9X,
  !     1'STATION', 21X,'LEVEL',/,19X,10I7)
  !     2211 FORMAT(//,12X,'SURFACE TEMPERATURE DATA')
  !     2212 FORMAT(5X,I3,'.  ',A4,5X,10F7.1)
2240 FORMAT(///,'   INPUT DATA AT TIME =  ',F6.0,' HOURS   ',4X, &
       ' (SIMULATION HOUR NO.      ',I3,' )')
2241 FORMAT(3X,64('-'))
2261 FORMAT(//,5X,'DIVERGENCE MINIMIZATION CRITERIA ',/,5X,32('-'), &
       /,5X,'MAXIMUM NUMBER OF ITERATIONS = ',I9)
2262 FORMAT(/,5X,'ACCEPTABLE DIVERGENCE LIMIT = ',E10.3)
2263 FORMAT(//,5X,'INTERPOLATION CRITERIA ',/,5X,22('-'),//, &
       5X,'INFLUENCE RADII OF STATION DATA:    MINIMUM = ',F7.2,/, &
       5X,'MAXIMUM SURFACE = ',F7.2,5X,'MAXIMUM UPPER = ',F7.2,/, &
       5X,'MAXIMUM WATER = ',F7.2)
2264 FORMAT(//,5X,'NUMBER OF STATIONS USED = ',5I5)
2265 FORMAT(//,5X,'IOBR = ',I5,5X,'I3DCTW = ',I5)
  !     2270 FORMAT(/,5X,'WIND DATA CONVERSION FACTORS',/,5X,28('-'),//,
  !     1 5X,'SURFACE:  ',5X,'SPEED = ',F6.2,5X,'POINTS = ',I4,//,
  !     2 5X,'UPPER AIR:',5X,'SPEED = ',F6.2,5X,'POINTS = ',I4)
2279 FORMAT(//,12X,'SURFACE WIND DATA ',/,12X,17('-'))
2280 FORMAT(/,' STATION   ','U-CMPT',5X,'V-CMPT',4X,'WEIGHTING', &
       /,11X,'M/SEC',6X,'M/SEC')
2290 FORMAT(2X,A4,F9.1,3X,F9.1,4X,F8.2)
2292 FORMAT(//,12X,'UPPER AIR WIND DATA  ',/,12X,19('-'),//,11X, &
       5('--- LEVEL ',I3,' ---    '))
2293 FORMAT(' STATION',3X,5('   U-CMPT   V-CMPT   '),/11X, &
       5('    M/S      M/S     '))
2294 FORMAT(2X,A4,7X,5(F5.1,4X,F5.1,7X))
  !     2295 FORMAT(/)
2390 FORMAT('  JOB ABORTED -- UNRECOGNIZED STATION NAME FOR UPPER AIR &
       &WIND DATA AT TIME = ',F6.0,'  NAME = ',A4)
2400 FORMAT(/,10X,'SMOOTHED WIND AND INITIAL DIVERGENCE FIELDS')
2401 FORMAT(//,20X,'MAXIMUM DIVERGENCE = ',E12.4)
2402 FORMAT(10X,43('-'))
  !     2405 FORMAT(/,5X,'INTERPOLATED STABILITY FIELD AT LEVEL = ',I4)
  !     2406 FORMAT(5X,42('-'))
2800 FORMAT(10X,'FINAL WIND FIELD AT TIME = ',F7.0,5X, &
       '(SIMULATION HOUR NO.',I3,' )')
2801 FORMAT(10X,63(1H-))
2803 FORMAT(/,10X,'FINAL DIVERGENCE FIELD AT TIME = ' ,F7.0,5X, &
       '(SIMULATION HOUR NO.',I3,' )',/,10X,66('-'))
  !     2809 FORMAT(//,5X,'SUMMARY OF DIVERGENCE MINIMIZATION'
  !     1,//,'  LEVEL   ITERATIONS   MAXIMUM DIVERGENCE (/SEC)')
  !     2810 FORMAT(3X,I2,I11,12X,E10.3)
2820 FORMAT(/,10X,'FINAL WIND SPEED AND DIRECTION AT TIME = ',F7.0, &
       '(SIMULATION HOUR NO.  ',I3)
2821 FORMAT(10X,72('-'))
  !     2891 FORMAT(/,5X,'GRIDDED CONFIDENCE FACTORS - SURFACE LEVEL')
  !     2892 FORMAT(/,5X,'GRIDDED CONFIDENCE FACTORS - UPPER LEVELS')
2900 FORMAT(' ',10X,'INTERPOLATED WIND FIELD')
2901 FORMAT(11X,23('-'))
2902 FORMAT(/,10X,'TERRAIN ADJUSTED SURFACE WINDS')
2903 FORMAT(10X,30('-'))
2904 FORMAT(/,10X,'KINEMATIC EFFECTS')
2905 FORMAT(10X,17('-'))
2906 FORMAT(/,10X,'FROUDE NUMBER EFFECTS')
2907 FORMAT(10X,21('-'))
2908 FORMAT(/,10X,'SLOPE EFFECTS')
2909 FORMAT(10X,13('-'))
3000 FORMAT(//,5X,'DIAGNOSTIC WIND MODEL PARAMETERS')
3010 FORMAT(5X,32('-'))
3012 FORMAT(/,5X,'R SURF =',F7.2,5X,'R UPPER = ',F7.2)
3015 FORMAT(/,5X,'GAMMA = ',8F6.3,/,13X,8F6.3,/,13X,8F6.3)
3020 FORMAT(/,5X,'TINF = ',F7.1)
3030 FORMAT(/,5X,'CRITFN = ',F7.1,5X,'TERRAD =',F7.1)
3040 FORMAT(/,5X,'BETA2 = ',8F5.2,/,13X,8F5.2,/,13X,8F5.2)
3050 FORMAT(/,5X,'IFRADJ = ',I7,5X,'IKINE = ',I7,5X,'ALPHA = ',F7.2)
3060 FORMAT(/,5X,'UM = ',8F5.1,/,10X,8F5.1,/,10X,8F5.1)
3070 FORMAT(/,5X,'VM = ',8F5.1,/,10X,8F5.1,/,10X,8F5.1)
  !     4010 FORMAT(20X,2F5.1)
  !
  stop
  !
  ! Errors
3001 write(*,'(''Error: Cannot open file: '',a)') TRIM(wdir)//'/'//'diagno.inp'
  stop
3002 write(*,'(''Error: Cannot open file: '',a)') TRIM(wdir)//'/'//'surface.dat'
  stop
3003 write(*,'(''Error: Cannot open file: '',a)') TRIM(wdir)//'/'//'upper.dat'
  stop
end PROGRAM DIAGNO
