module m_fradj

contains

  SUBROUTINE FRADJ(U,V,HTOPO,HMAX,TIME)
    !
    !     DIAGNO  VERSION 1.1  LEVEL 900221  FRADJ  S.DOUGLAS, SAI
    !
    use m_domain

    real :: u(:,:,:), v(:,:,:), htopo(:,:), hmax(:,:)

    !
    !     THIS SUBROUTINE IS ADAPTED FROM THE MELSAR MODEL.
    !     THE LOCAL FROUDE NUMBER FOR EACH GRID POINT IS CALCULATED
    !     IF THIS EXCEEDS A CRITICAL VALUE AND IF THE WIND IS BLOWING
    !     TOWRD THE OBSTACLE, THE U AND V WIND COMPONENTS ARE
    !     ADJUSTED.
    !
    !     INPUT:  U (R ARRAY)      - GRIDDED X-DIRECTION WIND COMPONENTS
    !             V (R ARRAY)      - GRIDDED Y-DIRECTION WIND COMPONENTS
    !             HTOPO (R ARRAY)  - GRIDDED TERRAIN HEIGHTS
    !             TGAMMA (R ARRAY) - TEMPERATURE LAPSE RATES (HOURLY)
    !             TINF (R)         - DOMAIN REPRESENTATIVE TEMPERATURE
    !             CRITFN (R)       - CRITICAL FROUDE NUMBER
    !             HMAX (R ARRAY)   - MAXIMUM TERRAIN HEIGHTS WITHIN A
    !                                GIVEN RADIUS
    !             TIME (R)         - SIMULATION TIME
    !
    !     OUTPUT:  U (R ARRAY) - ADJUSTED X-DIRECTION WIND COMPONENTS
    !              V (R ARRAY) - ADJUSTED Y-DIRECTION WIND COMPONENTS
    !
    DATA ZERO /0./
    !
    !     COMPUTE THE STABILITY PARAMETER
    !
    TAU=-0.01
    IT = IFIX(TIME/100.) + 1
    TGAMMA2 = TGAMMA(IT) - TAU
    !
    !     MELSAR FROUDE NUMBER ADJUSTMENT
    !
    RPD=0.017453
    DPR=57.296
    DO J=1,NY
       DO I=1,NX
          !
          !     CALCULATE THE TOPOGRAPHIC GRADIENTS
          !
          DXI=0.5/DX
          DYI=0.5/DY
          HTOIM1=HTOPO(I,J)
          HTOJM1=HTOPO(I,J)
          IF (I .GT. 1) HTOIM1=HTOPO(I-1,J)
          IF (J .GT. 1) HTOJM1=HTOPO(I,J-1)
          HTOIP1=HTOPO(I,J)
          HTOJP1=HTOPO(I,J)
          IF (I .LT. NX) HTOIP1=HTOPO(I+1,J)
          IF (J .LT. NY) HTOJP1=HTOPO(I,J+1)
          DELHI=(HTOIP1-HTOIM1)*DXI
          DELHJ=(HTOJP1-HTOJM1)*DYI
          IF (ABS(DELHI) .LT. 0.00015 .AND. ABS(DELHJ) .LT. 0.00015) THEN
             DELHI = 0.
             DELHJ = 0.
          ENDIF
          !
          DO K=1,NZ
             !
             !     CALCULATE THE LOCAL FROUDE NUMBER
             !
             SPEED=sqrt((U(I,J,K)**2+V(I,J,K)**2))
             OBSHGT=HMAX(I,J)-(HTOPO(I,J)+CELLZB(K)+DZ(K)/2.)
             IF (OBSHGT .LE. 0.0) cycle
             IF (TGAMMA2 .LE. 0.0) cycle
             FROUDE=SPEED/((sqrt(9.8*TGAMMA2/TINF))*OBSHGT)
             IF (FROUDE .GT. CRITFN) cycle
             !
             !     SET UP DRAINAGE VECTOR DIRECTIONS
             !
             THET  = 0.0  ! Initialize
             THETP = 0.0  ! Initialize
             IF (DELHI .EQ. 0.) THEN
                IF (DELHJ .EQ. 0.) cycle
                IF (DELHJ .LT. 0.) THET=270.
                IF (DELHJ .GT. 0.) THET=90.
             ELSE
                THETP=ATAN(DELHJ/DELHI)*DPR
             ENDIF
             IF (DELHI .LT. 0.) THET=THETP+180.
             IF (DELHI .GT. 0.) THEN
                IF (DELHJ .GT. 0.) THEN
                   THET=THETP
                ELSE
                   THET=THETP+360.
                ENDIF
             ENDIF
             THETD = 0.0  ! Initialize
             IF (THET .GE. 0. .AND. THET .LE. 90) THETD=90.-THET
             IF (THET .GT. 90. .AND. THET .LE. 360) THETD=450.-THET
             !
             !     RESOLVE DRAINAGE VECTOR INTO COMPONENTS
             !
             ANG=(270.-THETD)*RPD
             DRX=-COS(ANG)
             DRY=-SIN(ANG)
             !
             !     ADJUST WINDS
             !     FIRST NORMALIZE WIND VECTOR
             !
             if(speed > 1e-10) then
                UN = U(I,J,K)/SPEED
                VN = V(I,J,K)/SPEED
             else
                UN = 0.0
                VN = 0.0
             end if
             !
             !     DETERMINE IF WIND IS BLOWING TOWARD OBSTACLE (AA>0)
             !
             AA=DRX*UN+DRY*VN
             IF (AA .LE. 0) cycle
             !
             !     DETERMINE TANGENT WIND VECTOR
             !
             TT=-DRY*UN+DRX*VN
             IF (TT .GT. 0.) THEN
                TAX=-DRY
                TAY=DRX
             ELSE
                TAX=DRY
                TAY=-DRX
             ENDIF
             U(I,J,K)=TAX*SPEED
             V(I,J,K)=TAY*SPEED
          end DO
       end DO
    end DO
    !
    RETURN
  END SUBROUTINE FRADJ

end module m_fradj
